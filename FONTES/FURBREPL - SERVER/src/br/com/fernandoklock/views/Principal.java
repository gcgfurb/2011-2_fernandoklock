package br.com.fernandoklock.views;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import br.com.fernandoklock.database.DBManipulate;
import br.com.fernandoklock.replication.ParameterC2DM;
import br.com.fernandoklock.replication.ParameterReplication;
import br.com.fernandoklock.replication.SocketServer;

public class Principal {

	public static void main(String[] args) {

		AtualizaDB db = null;

		ParameterReplication paramReplication = new ParameterReplication(15001,
				true, true, new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
				   0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f});


		ParameterC2DM paramC2DM = new ParameterC2DM("klockfernando@gmail.com","!nando04!", "GOOGLE");

		while (true) {
			// Monta MENU
			System.out.println("------- Server FurbRepl ------ \n");

			System.out.println("1 - Notifica dispositivo");
			System.out.println("2 - Inicia listener");
			System.out.println("3 - Lista Cursos");
			System.out.println("4 - Lista Disciplinas");
			System.out.println("5 - Lista Alunos");
			System.out.println("6 - Inserir Curso");
			System.out.println("7 - Alterar Curso");
			System.out.println("8 - Excluir Curso");
			System.out.println("9 - Inserir Disciplina");
			System.out.println("10 - Alterar Disciplina");
			System.out.println("11 - Excluir Disciplina");
			System.out.println("12 - Inserir Aluno");
			System.out.println("13 - Alterar Aluno");
			System.out.println("14 - Excluir Aluno");
			System.out.println("15 - Inserir Aluno em Disciplinas");
			System.out.println("16 - Alterar Aluno em Disciplinas");
			System.out.println("17 - Excluir Aluno em Disciplinas");
			System.out.println("18 - Lista Erros");
			System.out.println("19 - Limpa Erros");
			System.out
					.println("20 - Alterar Sentido da Replicacao (SOURCE / TARGET)");
			System.out.println("21 - Alterar chave de Criptografia");
			System.out.println("22 - Listar Alunos com Presenca");
			System.out.println("23 - Limpar Registros C2DM");
			System.out.println("24 - Sair");

			System.out.println("");
			System.out.print("Opcao: ");

			// Leitura Console
			Scanner scan = new Scanner(System.in);

			String opMenu = scan.nextLine();

			int opcao = 0;

			// Caso usuario entre com dados que nao sao integer - EXCEPTION
			try {
				opcao = Integer.parseInt(opMenu);
			} catch (Exception ex) {
				System.out
						.println("Opcao errada! - Selecione a opcao do MENU. \n");
				continue;
			}

			switch (opcao) {
			case 1: {
				try {
					db.notifyForReplication();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				break;
			case 2: {
				db.createListenerReplication();
			}
				break;
			case 3: {
				ArrayList<String> array = db.selectReg("Curso",
						"nmCurso,idCurso", "");

				for (String s : array) {
					System.out.println(s);
				}
			}
				break;
			case 4: {
				ArrayList<String> array = db.selectReg("Disciplina",
						"nmDisciplina,idDisciplina", "");

				for (String s : array) {
					System.out.println(s);
				}
			}
				break;

			case 5: {
				ArrayList<String> array = db.selectReg("Aluno",
						"nmAluno,idAluno", "");

				for (String s : array) {
					System.out.println(s);
				}
			}
				break;
			case 6: {
				System.out.println("");
				System.out.println("Nome do curso: ");

				Scanner scanCurso = new Scanner(System.in);
				String nomeCurso = scanCurso.nextLine();
				try {
					db.insertReg("Curso", "nmCurso", "'" + nomeCurso + "'");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				break;
			case 7: {

				System.out.println("");
				System.out.println("ID do Curso: ");

				Scanner scanDisciplina = new Scanner(System.in);
				String idCurso = scanDisciplina.nextLine();

				System.out.println("");
				System.out.println("Nome do curso: ");
				String nomeCurso = scanDisciplina.nextLine();

				try {
					db.updateReg("Curso", "nmCurso = '" + nomeCurso + "' ",
							"idCurso = " + idCurso);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
				break;
			case 8: {
				System.out.println("");
				System.out.println("ID do curso: ");

				Scanner scanCurso = new Scanner(System.in);
				String idCurso = scanCurso.nextLine();
				try {
					db.deleteReg("Curso", "where idCurso = " + idCurso);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
				break;
			case 9: {
				System.out.println("");
				System.out.println("Nome do disciplina: ");

				Scanner scanDisciplina = new Scanner(System.in);
				String nomeDisciplina = scanDisciplina.nextLine();

				System.out.println("");
				System.out.println("ID do Curso da Disciplina: ");
				String idCurso = scanDisciplina.nextLine();
				try {
					db.insertReg("Disciplina", "nmDisciplina, idCurso", "'"
							+ nomeDisciplina + "' , " + idCurso);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
				break;
			case 10: {

				System.out.println("");
				System.out.println("ID da Disciplina: ");

				Scanner scanDisciplina = new Scanner(System.in);
				String idDisciplina = scanDisciplina.nextLine();

				System.out.println("");
				System.out.println("Nome da Disciplina: ");
				String nomeDisciplina = scanDisciplina.nextLine();

				try {
					db.updateReg("Disciplina", "nmDisciplina = '"
							+ nomeDisciplina + "'", " where idDisciplina = "
							+ idDisciplina);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
				break;
			case 11: {
				System.out.println("");
				System.out.println("ID da disciplina: ");

				Scanner scanDisciplina = new Scanner(System.in);
				String idDisciplina = scanDisciplina.nextLine();
				try {
					db.deleteReg("Disciplina", "where idDisciplina = "
							+ idDisciplina);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
				break;
			case 12: {
				System.out.println("");
				System.out.println("Nome do Aluno: ");

				Scanner scanAluno = new Scanner(System.in);
				String nomeAluno = scanAluno.nextLine();
				try {
					db.insertReg("Aluno", "nmAluno", "'" + nomeAluno + "'");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				break;
			case 13: {
				System.out.println("");
				System.out.println("ID do Aluno: ");

				Scanner scanDisciplina = new Scanner(System.in);
				String idAluno = scanDisciplina.nextLine();

				System.out.println("");
				System.out.println("Nome do Aluno: ");
				String nomeAluno = scanDisciplina.nextLine();

				try {
					db.updateReg("Aluno", "nmAluno = '" + nomeAluno + "'",
							" where idAluno = " + idAluno);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
				break;
			case 14: {
				System.out.println("");
				System.out.println("ID do Aluno: ");

				Scanner scanAluno = new Scanner(System.in);
				String idAluno = scanAluno.nextLine();
				try {
					db.deleteReg("Aluno", "where idAluno = " + idAluno);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				break;
			case 15: {
				System.out.println("");
				System.out.println("ID da Disciplina: ");

				Scanner scanDisciplinaAluno = new Scanner(System.in);
				String idDisciplina = scanDisciplinaAluno.nextLine();

				System.out.println("");
				System.out.println("ID do Aluno: ");

				String idAluno = scanDisciplinaAluno.nextLine();
				try {
					db.insertReg("DisciplinaAluno", "idAluno, idDisciplina",
							idAluno + " , " + idDisciplina);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
				break;
			case 16: {
				System.out.println("");
				System.out.println("ID da Disciplina: ");

				Scanner scanDisciplina = new Scanner(System.in);
				String idCurso = scanDisciplina.nextLine();

				System.out.println("");
				System.out.println("ID do Aluno: ");
				String nomeCurso = scanDisciplina.nextLine();

				try {
					db.updateReg("Curso", "nmCurso = '" + nomeCurso + "'",
							" where idCurso = " + idCurso);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
				break;
			case 17: {
				System.out.println("");
				System.out.println("ID da Disciplina: ");

				Scanner scanDisciplinaAluno = new Scanner(System.in);
				String idDisciplina = scanDisciplinaAluno.nextLine();

				System.out.println("");
				System.out.println("ID do Aluno: ");

				String idAluno = scanDisciplinaAluno.nextLine();
				try {
					db.updateReg("DisciplinaAluno", "idAluno = " + idAluno
							+ " , idDisiplina = " + idDisciplina,
							" where idAluno = " + idAluno
									+ " and idDisciplina = " + idDisciplina);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				break;
			case 18: {
				ArrayList<String> result = db.getReplicationError();

				for (String s : result) {
					System.out.println(s);
				}
			}
				break;
			case 19: {
				db.clearReplicationError();
			}
				break;
			case 20: {
				System.out.println("");
				System.out.println("Escolha - SOURCE ou TARGET: ");

				Scanner scanDatabase = new Scanner(System.in);
				String tpDataBase = scanDatabase.nextLine();

				if (tpDataBase.equalsIgnoreCase("target")) {
					paramReplication.setPassiveServer(true);
				} else {
					paramReplication.setPassiveServer(false);
				}

				try {
					db = new AtualizaDB("FurbDB", paramReplication, paramC2DM);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				break;
			case 21: {
				System.out.println("Escolha a chave de criptografia:");
				System.out.println("1 - Sem Criptografia");
				System.out.println("2 - 128bits");
				System.out.println("3 - 192bits");
				System.out.println("4 - 256bits");

				// Leitura Console
				Scanner scanCripto = new Scanner(System.in);

				String opMenuCripto = scanCripto.nextLine();

				int opcaoCripto = 0;

				// Caso usuario entre com dados que nao sao integer - EXCEPTION
				try {
					opcaoCripto = Integer.parseInt(opMenuCripto);
				} catch (Exception ex) {
					System.out
							.println("Opcao errada! - Selecione a opcao do MENU. \n");
					continue;
				}

				switch (opcaoCripto) {
				case 1: {
					paramReplication.setActiveCrypto(false);
				}
					break;
				case 2: {
					paramReplication.setActiveCrypto(true);
					paramReplication.setKeyCrypto(new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
							   0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f});
				}
					break;
				case 3: {
					paramReplication.setActiveCrypto(true);
					paramReplication.setKeyCrypto(new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
							  0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
							  0x0f,0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07 });
				}
					break;
				case 4: {
					paramReplication.setActiveCrypto(true);
					paramReplication.setKeyCrypto(new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
							   0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
							   0x0f, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
							   0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07});
				}
					break;
				default: {
					continue;
				}

				}

			}
				break;
			case 22: {
				System.out.println("");
				System.out.println("ID da Disciplina: ");

				Scanner scanDisciplinaAluno = new Scanner(System.in);
				String idDisciplina = scanDisciplinaAluno.nextLine();

				ArrayList<String> result = null;

				try {
					result = db.selectReg(" FaltaAluno , Aluno ",
							" nmAluno , dataFalta ",
							" where FaltaAluno.idDisciplina = " + idDisciplina
									+ "  and "
									+ " FaltaAluno.idAluno = Aluno.idAluno");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println("");
				System.out.println("Aluno          Data da Presen�a");

				if (result != null) {
					for (String s : result) {
						System.out.println(s);
					}
				}

			}
				break;

			case 23: {
				db.clearRegistryC2DM();
			}
				break;

			case 24: {
				System.exit(0);
			}
				break;
			}
		}
	}

}
