package br.com.fernandoklock.views;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;


import br.com.fernandoklock.database.DBManipulate;
import br.com.fernandoklock.replication.*;

public class AtualizaDB extends DBManipulate {

	
	
	public AtualizaDB(String file, ParameterReplication paramReplication, ParameterC2DM paramC2DM)
			throws SQLException, ClassNotFoundException, Exception {
		super(file, paramReplication, paramC2DM);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public void insertReg(String table, String colunsName, String values)
			throws Exception {
		// TODO Auto-generated method stub
		super.insertReg(table, colunsName, values);
	}
	
	@Override
	public void updateReg(String table, String setAtrib, String where)
			throws Exception {
		// TODO Auto-generated method stub
		super.updateReg(table, setAtrib, where);
	}
	
	@Override
	public void deleteReg(String table, String where) throws Exception {
		// TODO Auto-generated method stub
		super.deleteReg(table, where);
	}
	
	@Override
	public ArrayList<String> selectReg(String tables, String projection,
			String where) {
		// TODO Auto-generated method stub
		return super.selectReg(tables, projection, where);
	}
	
	@Override
	public void createListenerReplication() {
		// TODO Auto-generated method stub
		super.createListenerReplication();
	}
	
	@Override
	public void deleteListenerReplication() {
		// TODO Auto-generated method stub
		super.deleteListenerReplication();
	}
	
	@Override
	public void notifyForReplication() throws Exception {
		// TODO Auto-generated method stub
		super.notifyForReplication();
	}
	
}
