package br.com.fernandoklock.replication;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import br.com.fernandoklock.crypto.CryptographyAES;
import br.com.fernandoklock.database.DBManipulate;

public class SocketServer extends Thread {
	
	private ManagerReplication replManager;
	private ParameterReplication paramReplication;
	private ArrayList<String> dadosSync;

	public SocketServer(ManagerReplication replManager, ParameterReplication paramReplication) {
		this.paramReplication = paramReplication;
		this.replManager = replManager;
	}

	@Override
	public void run() {
		
		ServerSocket echoServer = null;
		DataInputStream is;
		DataOutputStream os = null;
		Socket serverSocket = null;

		try {
			echoServer = new ServerSocket(this.paramReplication.getNrPortHost());

		} catch (IOException e) {
			System.out.println(e);
		}
		while (true) {
			try {

				serverSocket = echoServer.accept();

				is = new DataInputStream(serverSocket.getInputStream());
				os = new DataOutputStream(serverSocket.getOutputStream());

				ArrayList<String> dadosLidos = new ArrayList<String>();

				String dadoLido = is.readUTF();
				
				// Consistencia para o C2DM
				if(dadoLido.equals("C2DMRegister")){
					dadoLido = is.readUTF();
					
					this.replManager.addRegistryAndroidC2DM(dadoLido);
					
					is.close();
					os.close();
					
					continue;
				}
				
				// Consistencia para a seguranca da criptografia	
				if (dadoLido.equals(this.paramReplication.tamanhoChave())) {
				
					os.writeUTF("OK");
					
				} else {
					
					os.writeUTF("NOK");
					
					is.close();
					os.close();
					
					continue;
				}
				
				dadoLido = is.readUTF();
					
				// Consistencia para a replica��o do banco	
				if ((dadoLido.equals("BeginReplicationSource") && this.paramReplication.isPassiveServer())
						|| (dadoLido.equals("BeginReplicationTarget") && !this.paramReplication.isPassiveServer())) {
				
					os.writeUTF("OK");
					
				} else {
					
					os.writeUTF("NOK");
					
					is.close();
					os.close();
					
					continue;
				}

				if (!this.paramReplication.isPassiveServer()) {
					// SERVIDOR COMO BANCO DE DADOS SOURCE

					boolean errorReplication = false;
					dadosSync = this.replManager.retDataSync();

					for (String dadoSync : this.dadosSync) {
						os.writeUTF(dadoSync);
					}

					os.writeUTF("EndOfTransmission");

					do {
						dadoLido = is.readUTF();

						if (dadoLido.equals("EndOfTransmission")) {
							break;
						} else if (dadoLido.equals("BeginOfTransmissionError")) {
							dadosLidos.clear();
						} else if (dadoLido.equals("EndOfTransmissionError")) {
							errorReplication = true;
							break;
						} else {
							dadosLidos.add(dadoLido);
						}

					} while (true);

					// Atualiza o banco DBReplication com a situa��o ocorrida
					this.replManager.returnReplication(errorReplication,
							dadosLidos);

				} else {
					// SERVIDOR COMO BANCO DE DADOS TARGET

					do {
						dadoLido = is.readUTF();

						if (dadoLido.equals("EndOfTransmission")) {
							break;
						}

						System.out.println(dadoLido);
						
						dadosLidos.add(dadoLido);

					} while (true);

					// APLICA REGISTROS
					ArrayList<String> errorAddTarget = this.replManager
							.addRegistryBaseTarget(dadosLidos);

					int errors = errorAddTarget.size();

					// VERIFICAR SE HOUVE ERRO AO INSERIR OS REGISTROS
					if (errors > 0) {
						os.writeUTF("BeginOfTransmissionError");

						for (String errorDB : errorAddTarget) {
							os.writeUTF(errorDB);
						}

						os.writeUTF("EndOfTransmissionError");

					} else {
						os.writeUTF("EndOfTransmission");
					}
				}

				if (dadosSync != null) {
					dadosSync.clear();
				}

				is.close();
				os.close();

			} catch (IOException e) {
				continue;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public ArrayList<String> getDadosSync() {
		return dadosSync;
	}

	public void setDadosSync(ArrayList<String> dadosSync) {
		this.dadosSync = dadosSync;
	}

}
