package br.com.fernandoklock.replication;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class NotifyClientC2DM {

	private static final String AUTH = "authentication";

	private static final String UPDATE_CLIENT_AUTH = "Update-Client-Auth";

	public static final String PARAM_REGISTRATION_ID = "registration_id";

	public static final String PARAM_DELAY_WHILE_IDLE = "delay_while_idle";

	public static final String PARAM_COLLAPSE_KEY = "collapse_key";

	private static final String UTF8 = "UTF-8";

	// Registration is currently hardcoded
	private ParameterC2DM paramC2DM;
	private ManagerReplication replManager;

	public NotifyClientC2DM(ParameterC2DM paramC2DM,
			ManagerReplication replManager) {
		this.paramC2DM = paramC2DM;
		this.replManager = replManager;
	}

	public String getAuthentification() {

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("https://www.google.com/accounts/ClientLogin");
		try {

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("Email", this.paramC2DM.getEmail()));
			nameValuePairs.add(new BasicNameValuePair("Passwd", this.paramC2DM.getPasswd()));
			nameValuePairs.add(new BasicNameValuePair("accountType",this.paramC2DM.getAccountType()));
			nameValuePairs.add(new BasicNameValuePair("source","Google-cURL-Example"));
			nameValuePairs.add(new BasicNameValuePair("service", "ac2dm"));

			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			
			while ((line = rd.readLine()) != null) {
				if (line.startsWith("Auth=")) {

					return line.substring(5, line.length());
				}

			}
		} catch (Exception e) {
			return "";
		}

		return "";
	}

	public void sendMessage() throws Exception {

		ArrayList<String> authDispositivos = this.replManager.retRegistryAndroidC2DM();

		if (authDispositivos == null)
			return;

		for (String auth_android : authDispositivos) {

			System.out.println("Started");
			String auth_key = getAuthentification();

			if (auth_key.trim().equalsIgnoreCase("")) {
				throw new Exception("Nao foi possivel autenticar no servidor da google.");
			}

			// Send a sync message to this Android device.
			StringBuilder postDataBuilder = new StringBuilder();
			postDataBuilder.append(PARAM_REGISTRATION_ID).append("=").append(auth_android);

			postDataBuilder.append("&").append(PARAM_COLLAPSE_KEY).append("=").append("0");

			postDataBuilder.append("&").append("data.payload").append("=").append(URLEncoder.encode("Lars war hier", UTF8));

			byte[] postData = postDataBuilder.toString().getBytes(UTF8);

			// Hit the dm URL.
			URL url = new URL("http://android.clients.google.com/c2dm/send");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length",Integer.toString(postData.length));
			conn.setRequestProperty("Authorization", "GoogleLogin auth=" + auth_key);

			OutputStream out = conn.getOutputStream();
			out.write(postData);
			out.close();

			int responseCode = conn.getResponseCode();

			System.out.println(String.valueOf(responseCode));
			// Validate the response code

			if (responseCode == 401 || responseCode == 403) {
				throw new Exception("C2DM " + "Unauthorized - need token");
			}

			// Check for updated token header
			String updatedAuthToken = conn.getHeaderField(UPDATE_CLIENT_AUTH);
			if (updatedAuthToken != null && !auth_key.equals(updatedAuthToken)) {
				throw new Exception("C2DM "
						+ "Got updated auth token from datamessaging servers: "
						+ updatedAuthToken);
			}

			String responseLine = new BufferedReader(new InputStreamReader(conn.getInputStream())).readLine();
			
			if (responseLine == null || responseLine.equals("")) {
				throw new IOException("Got empty response from Google AC2DM endpoint.");
			}

			String[] responseParts = responseLine.split("=", 2);
			
			if (responseParts.length != 2) {
				throw new IOException("Invalid response from Google "
									  + responseCode + " " + responseLine);
			}

			if (responseParts[0].equals("id")) {
				System.out.println("Successfully sent data message to device: " + responseLine);
			}

			if (responseParts[0].equals("Error")) {
				String err = responseParts[1];
				System.out.println("C2DM Got error response from Google datamessaging endpoint: "
								   + err);
				throw new IOException(err);
			}
		}
	}
}