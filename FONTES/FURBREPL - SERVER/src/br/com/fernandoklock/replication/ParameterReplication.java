package br.com.fernandoklock.replication;

public class ParameterReplication {
	
	private int nrPortHost;
	
	private boolean activeCrypto;
	
	private boolean passiveServer;
	
	private byte[] keyCrypto;
	
	public ParameterReplication(int nrPortHost,  boolean activeCrypto, boolean passiveServer, byte[] keyCrypto) {
		this.nrPortHost = nrPortHost;
		this.activeCrypto = activeCrypto;
		this.keyCrypto = keyCrypto;
		this.passiveServer = passiveServer;
	}
	
	public int getNrPortHost() {
		return nrPortHost;
	}
	
	public boolean isActiveCrypto() {
		return activeCrypto;
	}
	
	public void setActiveCrypto(boolean activeCrypto) {
		this.activeCrypto = activeCrypto;
	}

	public boolean isPassiveServer() {
		return passiveServer;
	}
	
	public void setPassiveServer(boolean passiveServer) {
		this.passiveServer = passiveServer;
	}

    public byte[] getKeyCrypto() {
		return keyCrypto;
	}
    
    public void setKeyCrypto(byte[] keyCrypto) {
		this.keyCrypto = keyCrypto;
	}
    
    public String tamanhoChave(){
    	
    	int tamVet = this.keyCrypto.length;
    	
    	if(!activeCrypto){
    		return "0";
    	}else if(tamVet == 16){
    		return "128";
    	}else if(tamVet == 24){
    		return "192";
    	}else if(tamVet == 32){
    		return "256";
    	}
    	
    	return "0";
    }
}

