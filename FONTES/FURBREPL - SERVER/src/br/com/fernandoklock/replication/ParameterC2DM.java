package br.com.fernandoklock.replication;

public class ParameterC2DM {

	private String email;
	private String passwd;
	private String accountType;

	public ParameterC2DM(String email, String passwd, String accountType){

		this.email = email;
		this.passwd = passwd;
		this.accountType = accountType;
	}

	public String getAccountType() {
		return accountType;
	}

	public String getEmail() {
		return email;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
}
