package br.com.fernandoklock.replication;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Pattern;

import br.com.fernandoklock.crypto.CryptographyAES;
import br.com.fernandoklock.database.DBManipulate;

public class ManagerReplication {

	private ParameterReplication paramReplication;
	private ParameterC2DM paramC2DM;
	private DBManipulate dbManipulate;

	private CryptographyAES crypto;
	private SocketServer serverSocket;

	private ArrayList<String> dadosSync;

	private Statement stmDBReplication;
	private Connection connDBReplication;

	public ManagerReplication(ParameterReplication paramReplication,
			ParameterC2DM paramC2DM, DBManipulate dbManipulate)
			throws Exception {

		this.paramReplication = paramReplication;
		this.paramC2DM = paramC2DM;
		this.dbManipulate = dbManipulate;

		this.connDBReplication = DriverManager
				.getConnection("jdbc:sqlite:ReplicationDB");
		this.stmDBReplication = this.connDBReplication.createStatement();

		if (this.paramReplication == null) {
			throw new Exception(
					"ManagerReplication Objeto ParamterReplication nao encontrado");
		}

		this.crypto = new CryptographyAES(this.paramReplication.getKeyCrypto());

		this.serverSocket = new SocketServer(this, this.paramReplication);

	}

	private void sendMessageReplication() throws Exception {
		NotifyClientC2DM clientC2DM = new NotifyClientC2DM(this.paramC2DM, this);
		clientC2DM.sendMessage();
	}

	// Adicionar registro na base de replica��o para ser enviado
	public void addRegistryReplication(String registry) throws Exception {

		// Se for Target nao adiciona na replica��o
		if (this.paramReplication.isPassiveServer())
			return;

		// caso o insert tenho VARCHAR, retirar a aspas duplas para nao dar erro
		// ao adicionar registro
		registry = registry.replace("\"", "'");

		try {
			
			this.stmDBReplication = this.connDBReplication.createStatement();
			this.stmDBReplication
					.executeUpdate("Insert into Registry (dsRegistry, flgSync) values ( \""
							+ registry + "\" , 0)");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Atualiza registros informando que o mesmo apresentou erro ao ser
	// executado na base de destino
	private void updateRegistryError(int idRegistry) throws Exception {

		try {
			this.stmDBReplication = this.connDBReplication.createStatement();
			this.stmDBReplication
					.executeUpdate("UPDATE Registry SET flgSync = 2 WHERE idRegistry = "
							+ idRegistry);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void startReplication() throws Exception {
		sendMessageReplication();
	}

	public ArrayList<String> retDataSync() {

		//atualiza chave da criptografia
		this.crypto.setChave(this.paramReplication.getKeyCrypto());
		
		ArrayList<String> dadosCryptoSync = new ArrayList<String>();
		this.dadosSync = retRegistryReplication();

		if (this.paramReplication.isActiveCrypto()) {

			// Criptografa as mensagens;
			for (String dados : this.dadosSync) {

				System.out.println("");
				System.out.println(dados);
				System.out.println("");
				
				if (paramReplication.isActiveCrypto()) {

					try {
						dadosCryptoSync.add(this.crypto.cifrar(dados));
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {

					dadosCryptoSync.add(dados);

				}

			}

			return dadosCryptoSync;

		}

		return dadosSync;

	}

	public void createListenerReplication() {
		this.serverSocket.start();
	}

	public void deleteListenerReplication() {
		this.serverSocket.stop();
	}

	// Retorna registros que ser�o replicados para a base de destino
	private ArrayList<String> retRegistryReplication() {

		ArrayList<String> resultado = new ArrayList<String>();

		try {
			this.stmDBReplication = this.connDBReplication.createStatement();

			ResultSet result = this.stmDBReplication
					.executeQuery("Select dsRegistry, idRegistry from Registry Where Registry.flgSync = 0");

			while (result.next()) {
				resultado.add(result.getString("idRegistry") + ";"
						+ result.getString("dsRegistry"));
			}

		} catch (SQLException e) {
			System.out
					.println("Class ManagerReplication - Erro no m�todo retRegistryreplication : "
							+ e);
		}

		return resultado;
	}

	// Retorna os registros que apresentaram erros ao serem inseridos na base de
	// destino
	public ArrayList<String> errorRegistryReplication() {

		ArrayList<String> resultado = new ArrayList<String>();

		try {
			this.stmDBReplication = this.connDBReplication.createStatement();

			ResultSet result = this.stmDBReplication
					.executeQuery("Select dsRegistry from Registry Where Registry.flgSync = 2");

			while (result.next()) {
				resultado.add(result.getString("dsRegistry"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultado;
	}

	// Limpa registros que j� foram enviados para a base de destino
	private void clearRegistryReplicationSended() {
		try {
			this.stmDBReplication = this.connDBReplication.createStatement();

			this.stmDBReplication
					.executeUpdate("DELETE FROM Registry WHERE Registry.flgSync = 0");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Limpa os registros da base de replica��o que contem erros
	public void clearRegistryReplicationError() {
		try {
			this.stmDBReplication = this.connDBReplication.createStatement();

			this.stmDBReplication
					.executeUpdate("DELETE FROM Registry WHERE Registry.flgSync = 2");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Campo Registry.flgSync --- 0 = nao enviado --- 1 = enviado --- 2 = error
	public void returnReplication(boolean errorReplication,
			ArrayList<String> regErrorReplication) {
		
		//atualiza chave da criptografia
		this.crypto.setChave(this.paramReplication.getKeyCrypto());
		
		if (errorReplication) {
			// Se ocorreu erro, assinalar quais registros apresentaram problemas
			for (String regError : regErrorReplication) {

				String regDecrypt = "";

				if (paramReplication.isActiveCrypto()) {

					try {
						regDecrypt = this.crypto.decifrar(regError);
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {
					regDecrypt = regError;
				}

				String vet[] = regDecrypt.split(";");

				if (vet.length < 2) {
					continue;
				}

				try {
					updateRegistryError(Integer.parseInt(vet[0]));
				} catch (Exception e) {
					System.out
							.println("Class ManagerReplication - Erro no ao executar m�todo updateRegistryError ");
				}
			}
		}

		// Limpa os registros que nao apresentaram error
		clearRegistryReplicationSended();
	}

	// Aplica os registros que vieram na replica��o
	public ArrayList<String> addRegistryBaseTarget(ArrayList<String> regsTarget) {

		ArrayList<String> errorAdd = new ArrayList<String>();
		Connection connDB = this.dbManipulate.getConnDB();
		Statement stmDB = null;
		
		//atualiza chave da criptografia
		this.crypto.setChave(this.paramReplication.getKeyCrypto());

		try {
			stmDB = connDB.createStatement();
		} catch (SQLException e) {
			System.out
					.println("Class ManagerReplicaton - Erro instanciar Statement para o banco Target : "
							+ e);
		}

		for (String regTarget : regsTarget) {

			String regDecrypt = "";

			if (paramReplication.isActiveCrypto()) {

				
				// Caso as chaves de criptografia forem diferentes, o frameowork retorna erro de replicacao
				try {
					regDecrypt = this.crypto.decifrar(regTarget);
				} catch (Exception e) {
					errorAdd.add(regTarget);
					continue;
				}

			} else {

				regDecrypt = regTarget;

			}
			
			// Se o cliente estiver enviado criptografado e o servidor n�o, ocorrera erro no split
			// Retornar erro de replicacao
			
			if(!Pattern.compile("update", Pattern.CASE_INSENSITIVE).matcher(regDecrypt).find() &&
			   !Pattern.compile("delete", Pattern.CASE_INSENSITIVE).matcher(regDecrypt).find() &&
			   !Pattern.compile("insert", Pattern.CASE_INSENSITIVE).matcher(regDecrypt).find()   ){
				errorAdd.add(regTarget);
				continue;
			}
			
			String[] vetDecrypt = regDecrypt.split(";");

			if (vetDecrypt.length < 2) {
				errorAdd.add(regTarget);
			}

			try {

				int res = stmDB.executeUpdate(vetDecrypt[1]);

				// N�o conseguiu aplicar nada na base de dados
				if (res == 0) {
					errorAdd.add(regTarget);
				}

			} catch (SQLException e) {
				errorAdd.add(regTarget);
			}
		}

		return errorAdd;
	}

	// Limpa os registros de registration ID enviados pelos Android
	public void clearRegistryAndroidC2DM() {
		try {
			this.stmDBReplication = this.connDBReplication.createStatement();

			this.stmDBReplication.executeUpdate("DELETE FROM AndroidC2DM");

		} catch (SQLException e) {
			System.out
					.println("Class ManagerReplication - Erro no m�todo clearRegistryAndroidC2DM: "
							+ e);
		}
	}

	// Adiciona os registros de registration ID enviados pelos Android
	public void addRegistryAndroidC2DM(String registration) {

		try {
			
			System.out.println("\n");
			System.out
					.println("Insert into AndroidC2DM (authAndroid) values ( \""
							+ registration + "\")");
			System.out.println("\n");
			
			this.stmDBReplication = this.connDBReplication.createStatement();
			this.stmDBReplication
					.executeUpdate("Insert into AndroidC2DM (authAndroid) values ( \""
							+ registration + "\")");

		} catch (SQLException e) {
			System.out
					.println("Class ManagerReplication - Erro no m�todo addRegistryAndroidC2DM : "
							+ e);
		}
	}

	// Retorna os registros de registration ID enviados pelos Android
	public ArrayList<String> retRegistryAndroidC2DM() {

		ArrayList<String> resultado = new ArrayList<String>();

		try {
			this.stmDBReplication = this.connDBReplication.createStatement();

			ResultSet result = this.stmDBReplication
					.executeQuery("Select authAndroid from AndroidC2DM");

			while (result.next()) {
				resultado.add(result.getString("authAndroid"));
			}

		} catch (SQLException e) {
			System.out
					.println("Class ManagerReplication - Erro no m�todo retRegistryAndroidC2DM : "
							+ e);
		}

		return resultado;
	}

}
