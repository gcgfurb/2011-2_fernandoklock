package br.com.fernandoklock.database;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;

import br.com.fernandoklock.replication.ManagerReplication;
import br.com.fernandoklock.replication.ParameterC2DM;
import br.com.fernandoklock.replication.ParameterReplication;

public abstract class DBManipulate {

	private Connection connDB;
	private Statement stmDB;

	private ManagerReplication replManager;

	/**
	 * O construtor cria uma nova conex�o com o banco de dados sqlite contido no
	 * arquivo passado como par�metro. A conex�o � possibilitada pelo driver
	 * JDBC, fornecido por SQLiteJDBC.
	 */
	public DBManipulate(String file, ParameterReplication paramReplication,
			ParameterC2DM paramC2DM) throws SQLException,
			ClassNotFoundException, Exception {

		Class.forName("org.sqlite.JDBC");

		this.connDB = DriverManager.getConnection("jdbc:sqlite:" + file);
		this.stmDB = this.connDB.createStatement();

		if (paramC2DM == null) {
			throw new Exception("Class ParameterC2DM nao foi instanciada.");
		}

		if (paramReplication == null) {
			throw new Exception(
					"Class ParameterReplication nao foi instanciada.");
		}

		try {
			this.replManager = new ManagerReplication(paramReplication,
					paramC2DM, this);
		} catch (Exception e) {
			System.out
					.println("Erro ao criar ManagerRepication - Class DBManipulate -> "
							+ e);
		}

	}

	public ArrayList<String> selectReg(String tables, String projection,
			String where) {

		ArrayList<String> resultado = new ArrayList<String>();

		try {
			this.stmDB = this.connDB.createStatement();

			ResultSet result = this.stmDB.executeQuery("Select " + projection
					+ " from " + tables + " " + where);

			String proj[] = null;

			// Final de linha
			projection += ",";
			proj = projection.split(",");

			while (result.next()) {

				String res = "";

				for (int i = 0; i < proj.length; i++) {
					res += result.getString(proj[i].trim()) + " ; ";
				}

				resultado.add(res);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultado;

	}

	/**
	 * Adiciona uma nova linha na tabela de recordes.
	 * 
	 * @param score
	 * @throws Exception
	 */
	public void insertReg(String table, String colunsName, String values)
			throws Exception {

		String registry = new String("");

		try {

			this.stmDB = this.connDB.createStatement();

			if (colunsName.equals("")) {
				this.stmDB.executeUpdate("INSERT INTO " + table + " VALUES ("
						+ values + ")");
				registry = "INSERT INTO " + table + " VALUES (" + values + ")";
			} else {
				System.out.println("INSERT INTO " + table + " ( " + colunsName
						+ " ) VALUES ( " + values + " )");
				this.stmDB.executeUpdate("INSERT INTO " + table + " ( "
						+ colunsName + " ) VALUES ( " + values + " )");
				registry = "INSERT INTO " + table + " ( " + colunsName
						+ " ) VALUES ( " + values + " )";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		this.replManager.addRegistryReplication(registry);

	}

	/**
	 * Remove a linha da tabela cuja coluna "jogo" seja igual a string passada
	 * como par�metro.
	 * 
	 * @param jogo
	 * @throws Exception
	 */
	public void deleteReg(String table, String where) throws Exception {

		try {
			this.stmDB = this.connDB.createStatement();

			this.stmDB.executeUpdate("DELETE FROM " + table + " " + where);

			this.replManager.addRegistryReplication("DELETE FROM " + table
					+ " " + where);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Altera o valor de uma linha do banco de dados.
	 * 
	 * @param hiScore
	 * @throws Exception
	 */
	public void updateReg(String table, String setAtrib, String where)
			throws Exception {
		try {

			System.out.println("UPDATE " + table + " SET " + setAtrib + " "
					+ where);
			this.stmDB = this.connDB.createStatement();
			this.stmDB.executeUpdate("UPDATE " + table + " SET " + setAtrib
					+ " " + where);

			this.replManager.addRegistryReplication("UPDATE " + table + " SET "
					+ setAtrib + " " + where);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> getReplicationError() {
		return this.replManager.errorRegistryReplication();
	}

	public void clearReplicationError() {
		this.replManager.clearRegistryReplicationError();
	}

	public void clearRegistryC2DM() {
		this.replManager.clearRegistryAndroidC2DM();
	}

	public void createListenerReplication() {
		this.replManager.createListenerReplication();
	}

	public void deleteListenerReplication() {
		this.replManager.deleteListenerReplication();
	}

	public void notifyForReplication() throws Exception {
		this.replManager.startReplication();
	}

	public Connection getConnDB() {
		return connDB;
	}

}
