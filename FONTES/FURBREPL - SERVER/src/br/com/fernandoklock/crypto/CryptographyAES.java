package br.com.fernandoklock.crypto;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class CryptographyAES {

	private static byte[] chave;

	public CryptographyAES(byte[] key) {
		this.chave = key;
	}

	public static String cifrar(String value) throws Exception{
		String retorno = null;

		SecretKeySpec spec = new SecretKeySpec(chave, "AES");

		AlgorithmParameterSpec paramSpec = new IvParameterSpec(new byte[16]);

		Cipher cifra = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cifra.init(Cipher.ENCRYPT_MODE, spec, paramSpec);

		byte[] cifrado = cifra.doFinal(value.getBytes());

		retorno = Base64.encodeBytes(cifrado);

		return retorno;
	}

	public static String decifrar(String cifra) throws Exception{
		String retorno = null;

		SecretKeySpec skeySpec = new SecretKeySpec(chave, "AES");

		AlgorithmParameterSpec paramSpec = new IvParameterSpec(new byte[16]);

		byte[] decoded = Base64.decode(cifra);

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, paramSpec);

		retorno = new String(cipher.doFinal(decoded));

		return retorno;
	}
	
	public static void setChave(byte[] chave) {
		CryptographyAES.chave = chave;
	}
	
	public static byte[] getChave() {
		return chave;
	}
	
}
