package br.com.fernandoklock.database;

import java.io.Serializable;
import java.util.ArrayList;

public class Data implements Serializable {
	
	// 0 - String, 1 - Integer, 2 - Boolean, 3 - Double
	private ArrayList<Integer> typeColumn;
	private ArrayList<String> nameColumn;
	private ArrayList<String> valueColumn;
	private int nrColumns;
	private String nameTable;
	
	public Data(ArrayList<Integer> typeColumn, ArrayList<String> nameColumn, ArrayList<String> valueColumn, String nameTable ) {
		this.typeColumn = typeColumn;
		this.nameColumn = nameColumn;
		this.valueColumn = valueColumn;	
		this.nrColumns = this.getNameColumn().size();
		this.nameTable = nameTable;
		
	}
	
	public String getNameTable() {
		return nameTable;
	}
	
	public void setNameTable(String nameTable) {
		this.nameTable = nameTable;
	}
	
	public ArrayList<String> getValueColumn() {
		return valueColumn;
	}
	
	public ArrayList<String> getNameColumn() {
		return nameColumn;
	}
	
	public ArrayList<Integer> getTypeColumn() {
		return typeColumn;
	}
	
	public void setValueColumn(ArrayList<String> valueColumn) {
		this.valueColumn = valueColumn;
	}
	
	public void setNameColumn(ArrayList<String> nameColumn) {
		this.nameColumn = nameColumn;
	}
	
	public void setTypeColumn(ArrayList<Integer> typeColumn) {
		this.typeColumn = typeColumn;
	}
	
	public int getNrColumns() {
		return nrColumns;
	}
	
	public void setNrColumns(int nrColumns) {
		this.nrColumns = nrColumns;
	}
}
