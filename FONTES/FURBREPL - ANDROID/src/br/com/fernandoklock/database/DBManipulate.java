package br.com.fernandoklock.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import br.com.fernandoklock.replication.ManagerReplication;
import br.com.fernandoklock.replication.ParameterC2DM;
import br.com.fernandoklock.replication.ParameterReplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public abstract class DBManipulate extends SQLiteOpenHelper implements
		Serializable {

	private SQLiteDatabase db;
	private ManagerReplication replManager;
	private Context context;
	private String dbName;
	private static String dbPath = "/data/data/br.com.fernandoklock.replication/databases/";

	public DBManipulate(String dbName, Context context,
			ParameterReplication paramReplication) throws IOException {
		super(context, dbName, null, 33);

		this.context = context;
		this.dbName = dbName;
		this.replManager = new ManagerReplication(context, paramReplication,
				this);

		createDataBase();

	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public void restoreDefaultDataBase() throws IOException {
		copyDataBase();
		replManager.copyDataBase();
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();

		if (dbExist) {
			close();
		} else {

			// By calling this method and empty database will be created into
			// the default system path
			// of your application so we are gonna be able to overwrite that
			// database with our database.
			this.getReadableDatabase();

			try {

				copyDataBase();

			} catch (IOException e) {

				throw new Error("Error copying database");
			}
		}

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {

		this.db = null;

		try {

			if (!databaseExist())
				return false;

			String myPath = this.dbPath + this.dbName;
			this.db = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);

		} catch (Exception e) {
			// database does't exist yet.
		}

		return this.db != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	public void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = context.getAssets().open(this.dbName);

		// Path to the just created empty db
		String outFileName = this.dbPath + this.dbName;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	private boolean databaseExist() {
		File dbFile = new File(dbPath + dbName);
		return dbFile.exists();
	}

	public ArrayList<String> selectReg(String tables, String projection,
			String where) throws Exception {

		open();

		// Inicia Array
		ArrayList<String> result = new ArrayList<String>();

		Cursor cur = db.rawQuery("Select " + projection + " from " + tables
				+ " " + where, null);

		if (!cur.moveToFirst()) {
			cur.close();
			close();
			return result;
		}

		// Quantidade de linhas e colunas encontradas
		int rows = cur.getCount();
		int columns = cur.getColumnCount();

		for (int row = 0; row < rows; row++) {

			String res = "";

			for (int column = 0; column < columns; column++) {
				res += cur.getString(column) + ";";
			}

			result.add(res);
			res = "";

			if (!cur.moveToNext()) {
				cur.close();
				close();
				return result;
			}
		}

		cur.close();
		close();

		return result;
	}

	public long insertReg(Data data) throws Exception {

		open();

		String SQLinsert = "Insert into " + data.getNameTable() + " ";

		String SQLcolumns = " ( ";
		String SQLvalues = " values ( ";

		ContentValues cv = null;

		cv = new ContentValues();

		String nameInsert = "";
		boolean boolNameInsert = true;

		for (int i = 0; i < data.getNrColumns(); i++) {

			int typeColumn = data.getTypeColumn().get(i);
			String nameColumn = data.getNameColumn().get(i);
			String valueColumn = data.getValueColumn().get(i);

			if (boolNameInsert) {
				nameInsert = nameColumn;
				boolNameInsert = false;
			}

			switch (typeColumn) {

			case 0: {
				cv.put(nameColumn, valueColumn);

				if (i == 0) {
					SQLcolumns += nameColumn;
					SQLvalues += " \"" + valueColumn + "\"";
				} else {
					SQLcolumns += " , " + nameColumn;
					SQLvalues += " , \"" + valueColumn + "\"";
				}
			}
				break;

			case 1: {
				cv.put(nameColumn, Integer.parseInt(valueColumn));

				if (i == 0) {
					SQLcolumns += nameColumn;
					SQLvalues += valueColumn;
				} else {
					SQLcolumns += " , " + nameColumn;
					SQLvalues += " , " + valueColumn;
				}
			}
				break;

			case 2: {
				cv.put(nameColumn, Boolean.parseBoolean(valueColumn));

				if (i == 0) {
					SQLcolumns += nameColumn;
					SQLvalues += valueColumn;
				} else {
					SQLcolumns += " , " + nameColumn;
					SQLvalues += " , " + valueColumn;
				}
			}
				break;

			case 3: {
				cv.put(nameColumn, Double.parseDouble(valueColumn));

				if (i == 0) {
					SQLcolumns += nameColumn;
					SQLvalues += valueColumn;
				} else {
					SQLcolumns += " , " + nameColumn;
					SQLvalues += " , " + valueColumn;
				}
			}
				break;

			} // Fim do Case

		} // Fim do FOR das columnas

		long retInsert = db.insert(data.getNameTable(), nameInsert, cv);

		SQLcolumns += " ) ";
		SQLvalues += " ) ";

		SQLinsert += SQLcolumns + SQLvalues;

		this.replManager.addRegistryReplication(SQLinsert);

		close();

		return retInsert;
	}

	public int deleteReg(String table, String where) throws Exception {

		open();

		SQLiteDatabase db = this.getWritableDatabase();
		int retDelete = db.delete(table, where, new String[] {});

		String SQLdelete = "Delete from " + table.toString() + " where "
				+ where;

		this.replManager.addRegistryReplication(SQLdelete);

		db.close();

		close();

		return retDelete;
	}

	public int updateReg(Data data, String where) throws Exception {

		open();

		String SQLupdate = "UPDATE " + data.getNameTable() + " SET ";

		ContentValues cv = null;

		cv = new ContentValues();

		for (int i = 0; i < data.getNrColumns(); i++) {

			int typeColumn = data.getTypeColumn().get(i);
			String nameColumn = data.getNameColumn().get(i);
			String valueColumn = data.getValueColumn().get(i);

			switch (typeColumn) {

			case 0: {
				cv.put(nameColumn, valueColumn);

				if (i == 0) {
					SQLupdate += nameColumn + " = \"" + valueColumn + "\"";
				} else {
					SQLupdate += " , " + nameColumn + " = \"" + valueColumn
							+ "\"";
				}
			}
				break;

			case 1: {
				cv.put(nameColumn, Integer.parseInt(valueColumn));

				if (i == 0) {
					SQLupdate += nameColumn + " = " + valueColumn + "";
				} else {
					SQLupdate += " , " + nameColumn + " = " + valueColumn;
				}
			}
				break;

			case 2: {
				cv.put(nameColumn, Boolean.parseBoolean(valueColumn));

				if (i == 0) {
					SQLupdate += nameColumn + " = " + valueColumn;
				} else {
					SQLupdate += " , " + nameColumn + " = " + valueColumn;
				}
			}
				break;

			case 3: {
				cv.put(nameColumn, Double.parseDouble(valueColumn));

				if (i == 0) {
					SQLupdate += nameColumn + " = " + valueColumn;
				} else {
					SQLupdate += " , " + nameColumn + " = " + valueColumn;
				}
			}
				break;

			} // Fim do Case

		} // Fim do FOR das columnas

		int retUpdate = db.update(data.getNameTable(), cv, where,
				new String[] {});

		if (!where.equalsIgnoreCase("")) {
			SQLupdate += " where " + where;
		}

		this.replManager.addRegistryReplication(SQLupdate);

		close();

		return retUpdate;
	}

	@Override
	public synchronized void close() {
		if (this.replManager.getDbReplication() == null || this.db == null)
			return;

		Log.e("CLOSE DB", "FECHAR O BANCO DE DADOS");
		this.replManager.close();
		this.db.close();

		this.replManager.setDbReplication(null);
		this.db = null;
	}

	public synchronized void open() {
		Log.e("OPEN DB", "ABRIR O BANCO DE DADOS");
		this.replManager.open(this.context);
		this.db = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE,
				null);
	}

	public synchronized boolean isOpen() {
		if (this.db == null && this.replManager.getDbReplication() == null)
			return false;

		Log.e("ISOPEN DB",
				"1 - " + this.db + "2 - " + this.replManager.getDbReplication());
		return true;
	}

	public ArrayList<String> getReplicationError() {
		open();
		ArrayList<String> error = this.replManager.errorRegistryReplication();
		close();
		return error;
	}

	public void clearReplicationError() {
		open();
		this.replManager.clearRegistryReplicationError();
		close();
	}

	public void startReplication() throws Exception {
		open();
		this.replManager.startReplication();
		close();
	}

	public SQLiteDatabase getDb() {
		return db;
	}
}
