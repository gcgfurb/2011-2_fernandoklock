package br.com.fernandoklock.ControlApplication;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import br.com.fernandoklock.database.DBManipulate;
import br.com.fernandoklock.database.Data;
import br.com.fernandoklock.replication.ParameterReplication;

public class AtualizaDB extends DBManipulate {

	public AtualizaDB( String dbName, Context context, ParameterReplication paramReplication) throws IOException {
		super( dbName,context, paramReplication);
	}
	
	@Override
	public int deleteReg(String table, String where) throws Exception {
		// TODO Auto-generated method stub
		return super.deleteReg(table, where);
	}
	@Override
	public long insertReg(Data data) throws Exception {
		// TODO Auto-generated method stub
		return super.insertReg(data);
	}
	
	@Override
	public int updateReg(Data data, String where) throws Exception {
		// TODO Auto-generated method stub
		return super.updateReg(data, where);
	}
	
	@Override
	public ArrayList<String> selectReg(String tables, String projection,
			String where) throws Exception {
		// TODO Auto-generated method stub
		return super.selectReg(tables, projection, where);
	}
	
	@Override
	public void clearReplicationError() {
		// TODO Auto-generated method stub
		super.clearReplicationError();
	}
	
	@Override
	public ArrayList<String> getReplicationError() {
		// TODO Auto-generated method stub
		return super.getReplicationError();
	}
	
}
