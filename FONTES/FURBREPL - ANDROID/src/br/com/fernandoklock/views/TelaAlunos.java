package br.com.fernandoklock.views;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import br.com.fernandoklock.ControlApplication.AtualizaDB;
import br.com.fernandoklock.ModelApplication.Aluno;
import br.com.fernandoklock.ModelApplication.Curso;
import br.com.fernandoklock.ModelApplication.Disciplina;
import br.com.fernandoklock.database.Data;
import br.com.fernandoklock.replication.ParameterReplication;
import br.com.fernandoklock.replication.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class TelaAlunos extends Activity {

	private ListView listaAlunos;
	private AtualizaDB db;
	private ArrayList<Aluno> alunos;
	private Disciplina disciplina;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.alunos);

		Intent intent = this.getIntent();
		this.disciplina = (Disciplina) intent
				.getSerializableExtra("Disciplina");

		ParameterReplication paramReplication = null;
		try {
			paramReplication = new ParameterReplication(this,
					"192.168.1.100", 15001, true, false, new byte[] { 0x00, 0x01,
							0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a,
							0x0b, 0x0c, 0x0d, 0x0e, 0x0f });
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			this.db = new AtualizaDB("FurbDB", this, paramReplication);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		openListView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.menu_aluno, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.add_aluno: {
			Intent intent = new Intent(TelaAlunos.this, TelaGenerica.class);
			intent.putExtra("ObjetoPadraoFilho", new Aluno(0, ""));
			intent.putExtra("ObjetoPadraoPai", this.disciplina);
			startActivity(intent);
		}
			break;
		case R.id.preferencias: {
			Intent intent = new Intent(TelaAlunos.this, TelaPreferencias.class);
			startActivity(intent);
		}
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		menu.setHeaderTitle(this.alunos.get(info.position).getNome());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.contextmenu_aluno, menu);

		boolean presenca = verificaPresencaAluno(this.alunos.get(info.position));

		if (presenca) {
			menu.getItem(2).setChecked(true);
		} 
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		final Aluno aluno = this.alunos.get(info.position);

		switch (item.getItemId()) {

		case R.id.alterarAluno: {
			Intent intent = new Intent(TelaAlunos.this, TelaGenerica.class);
			intent.putExtra("ObjetoPadraoFilho", aluno);
			intent.putExtra("ObjetoPadraoPai", this.disciplina);
			startActivity(intent);
		}
			break;

		case R.id.excluirAluno: {

			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setMessage("Deseja excluir o aluno " + aluno.getNome()
					+ " do sistema de chamada?");

			dialog.setPositiveButton("Sim",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							deleteAluno(aluno);
						}
					});

			dialog.setNegativeButton("N�o",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							return;
						}
					});

			dialog.setTitle("Aviso");
			dialog.show();

		}
			break;

		case R.id.chkFalta: {

			if (item.isChecked()) {
				excluiRegistroFalta(aluno);
			} else {
				inserirRegistroFalta(aluno);
			}

		}
			break;

		default:
			return super.onContextItemSelected(item);
		}

		openListView();

		return true;
	}

	
	@Override
	protected void onResume() {

		openListView();
		super.onResume();
	}

	public void openListView() {

		this.alunos = returnAlunos(disciplina);

		final String vetAlunos[] = new String[alunos.size()];

		int contAlunos = 0;

		for (Aluno a : alunos) {
			vetAlunos[contAlunos] = a.getNome();
			contAlunos++;
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, vetAlunos);

		listaAlunos = (ListView) findViewById(R.id.listaAlunos);
		listaAlunos.setAdapter(adapter);

		listaAlunos.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView arg0, View arg1, int arg2,
					long arg3) {
				System.out.println(listaAlunos.getItemAtPosition(arg2));
			}
		});
		registerForContextMenu(this.listaAlunos);
	}

	public ArrayList<Aluno> returnAlunos(Disciplina disciplina) {

		ArrayList<String> resultSelect = new ArrayList<String>();
		ArrayList<Aluno> alunos = new ArrayList<Aluno>();

		try {
			resultSelect = this.db
					.selectReg(
							"Disciplina, Aluno, DisciplinaAluno",
							"Aluno.idAluno,Aluno.nmAluno",
							" where Disciplina.idDisciplina = "
									+ disciplina.getId()
									+ " and "
									+ " DisciplinaAluno.idDisciplina = Disciplina.idDisciplina and "
									+ " DisciplinaAluno.idAluno = Aluno.idAluno");
		} catch (Exception e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (String s : resultSelect) {
			String split[] = s.split(";");
			alunos.add(new Aluno(Integer.parseInt(split[0]), split[1]));
		}

		return alunos;
	}

	public void deleteAluno(Aluno aluno) {

		try {
			this.db.deleteReg("Aluno", "idAluno = " + aluno.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			this.db.deleteReg("DisciplinaAluno",
					"idDisciplina = " + disciplina.getId() + " and "
							+ "idAluno = " + aluno.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		openListView();
	}

	public boolean verificaPresencaAluno(Aluno aluno) {

		ArrayList<String> resultSelect = new ArrayList<String>();

		String data = retornaDataAtual();

		try {
			resultSelect = this.db.selectReg("FaltaAluno",
					"FaltaAluno.idAluno", " where idAluno = " + aluno.getId()
							+ " and " + " idDisciplina = " + disciplina.getId()
							+ " and " + " dataFalta = '" + data + "'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSelect.size() < 1) {
			return false;
		}

		return true;
	}

	public void inserirRegistroFalta(Aluno aluno) {

		ArrayList<Integer> tpColumn = new ArrayList<Integer>();

		tpColumn.add(1);
		tpColumn.add(1);
		tpColumn.add(0);

		ArrayList<String> nmColumn = new ArrayList<String>();

		nmColumn.add("idAluno");
		nmColumn.add("idDisciplina");
		nmColumn.add("dataFalta");

		ArrayList<String> valuesColumn = new ArrayList<String>();

		valuesColumn.add(aluno.getId() + "");
		valuesColumn.add(disciplina.getId() + "");
		valuesColumn.add(retornaDataAtual());

		Data data = new Data(tpColumn, nmColumn, valuesColumn, "FaltaAluno");

		try {
			db.insertReg(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void excluiRegistroFalta(Aluno aluno) {

		String data = retornaDataAtual();

		try {
			this.db.deleteReg("FaltaAluno", "idAluno = " + aluno.getId()
					+ " and " + "idDisciplina = " + disciplina.getId()
					+ " and " + "dataFalta = '" + data + "'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String retornaDataAtual() {

		Calendar c = Calendar.getInstance();

		String data = "";

		if (c.get(Calendar.DAY_OF_MONTH) < 2) {
			data = "0" + c.get(Calendar.DAY_OF_MONTH) + "-";
		} else {
			data = c.get(Calendar.DAY_OF_MONTH) + "-";
		}

		if ((c.get(Calendar.MONTH) + 1) < 2) {
			data += "0" + (c.get(Calendar.MONTH) + 1) + "-";
		} else {
			data += (c.get(Calendar.MONTH) + 1) + "-";
		}

		data += c.get(Calendar.YEAR);

		return data;

	}

}
