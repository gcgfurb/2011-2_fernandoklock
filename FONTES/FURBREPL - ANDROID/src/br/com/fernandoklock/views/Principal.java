package br.com.fernandoklock.views;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import br.com.fernandoklock.ControlApplication.AtualizaDB;
import br.com.fernandoklock.replication.ParameterReplication;
import br.com.fernandoklock.replication.R;

public class Principal extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("ReplicationFramework");

		setContentView(R.layout.principal);

		Button btAplicacao = (Button) findViewById(R.id.btAplicacao);
		Button btLogAplicacao = (Button) findViewById(R.id.btLogAplicacao);
		Button btDefaultConf = (Button) findViewById(R.id.btDefaultConfig);

		btAplicacao.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Principal.this, TelaCursos.class);
				startActivity(intent);

			}
		});

		btLogAplicacao.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Principal.this,
						TelaErroReplicacao.class);
				startActivity(intent);
			}
		});

		btDefaultConf.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					restoreDefaultConfig();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public void restoreDefaultConfig() throws IOException {
		ParameterReplication paramReplication = null;

		paramReplication = new ParameterReplication(this, "", 15001, true,
				false, new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
						0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f });

		final AtualizaDB db = new AtualizaDB("FurbDB", this, paramReplication);

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setMessage("Deseja restaurar configura��o padr�o?");

		dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					db.restoreDefaultDataBase();
				} catch (IOException e) {
					AlertDialog.Builder dialogErro = new AlertDialog.Builder(
							Principal.this);
					dialogErro
							.setMessage("N�o foi possivel restaurar configura��o padr�o!");

					dialogErro.setNeutralButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									return;
								}
							});

					dialogErro.setTitle("Aviso");
					dialogErro.show();
				}
			}
		});

		dialog.setNegativeButton("N�o", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				return;

			}
		});

		dialog.setTitle("Aviso");
		dialog.show();

	}
}