package br.com.fernandoklock.views;

import java.io.IOException;
import java.util.ArrayList;

import br.com.fernandoklock.ControlApplication.AtualizaDB;
import br.com.fernandoklock.ModelApplication.Curso;
import br.com.fernandoklock.ModelApplication.Disciplina;
import br.com.fernandoklock.replication.ParameterReplication;
import br.com.fernandoklock.replication.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

public class TelaErroReplicacao extends Activity{
	
	private AtualizaDB db;
	private EditText textErro = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.erros);
		
		ParameterReplication paramReplication = null;
		try {
			paramReplication = new ParameterReplication(this,
					"192.168.1.100", 15001, true, false, new byte[] { 0x00,
							0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
							0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f });
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			this.db = new AtualizaDB("FurbDB", this, paramReplication);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.textErro = (EditText) findViewById(R.id.textErro);
		this.textErro.setEnabled(false);
		
		buscarErros();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.menu_erro, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {

		case R.id.erro_repl: {
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setMessage("Deseja limpar os erros retornados pela replica��o?");
			
			dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					db.clearReplicationError();
					textErro.setText("");
				}
			});
			
			dialog.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					return;
					
				}
			});
			
			dialog.setTitle("Aviso");
			dialog.show();
		}
			break;
		}

		return super.onOptionsItemSelected(item);
	}
	
	public void buscarErros(){
		ArrayList<String> erros = null;
		
		erros = db.getReplicationError();
		
		if (erros == null)
			erros = new ArrayList<String>();
		
		String lines = "";
		
		for(String erro: erros){
			lines += erro + "\n" + "\n";
		}
		
		textErro.setText(lines);
	}

}
