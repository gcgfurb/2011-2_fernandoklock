package br.com.fernandoklock.views;

import java.io.IOException;
import java.util.ArrayList;

import br.com.fernandoklock.ControlApplication.AtualizaDB;
import br.com.fernandoklock.ModelApplication.Aluno;
import br.com.fernandoklock.ModelApplication.Curso;
import br.com.fernandoklock.ModelApplication.Disciplina;
import br.com.fernandoklock.ModelApplication.ObjetoPadrao;
import br.com.fernandoklock.database.Data;
import br.com.fernandoklock.replication.ParameterReplication;
import br.com.fernandoklock.replication.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaGenerica extends Activity {

	private AtualizaDB db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.telaupdate);

		ParameterReplication paramReplication = null;
		try {
			paramReplication = new ParameterReplication(this,
					"192.168.1.100", 15001,  true, false, new byte[] { 0x00,
							0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
							0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f });
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			this.db = new AtualizaDB("FurbDB", this, paramReplication);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Intent intent = getIntent();

		final ObjetoPadrao obj = (ObjetoPadrao) intent
				.getSerializableExtra("ObjetoPadraoFilho");

		final ObjetoPadrao obj2 = (ObjetoPadrao) intent
				.getSerializableExtra("ObjetoPadraoPai");

		final EditText text = (EditText) findViewById(R.id.txtNome);
		text.setText(obj.getNome());

		Button btAplicar = (Button) findViewById(R.id.btAplicar);

		btAplicar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (obj.getId() == 0) {
					insertRegistro(obj, obj2, text.getText().toString());
				} else {
					updateRegistro(obj, text.getText().toString());
				}

				setResult(RESULT_OK, null);
				onDestroy();
				finish();
			}
		});

	}
	
	public void insertRegistro(ObjetoPadrao obj, ObjetoPadrao obj2,
			String novoObjetoPadrao) {

		switch (obj.getTipoObjeto()) {
		case 1: {

			ArrayList<Integer> tpColumn = new ArrayList<Integer>();

			tpColumn.add(0);

			ArrayList<String> nmColumn = new ArrayList<String>();

			nmColumn.add("nmCurso");

			ArrayList<String> valuesColumn = new ArrayList<String>();

			valuesColumn.add(novoObjetoPadrao);

			Data data = new Data(tpColumn, nmColumn, valuesColumn, "Curso");

			try {
				this.db.insertReg(data);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			break;
		case 2: {

			ArrayList<Integer> tpColumn = new ArrayList<Integer>();

			tpColumn.add(0);
			tpColumn.add(1);

			ArrayList<String> nmColumn = new ArrayList<String>();

			nmColumn.add("nmDisciplina");
			nmColumn.add("idCurso");

			ArrayList<String> valuesColumn = new ArrayList<String>();

			valuesColumn.add(novoObjetoPadrao);
			valuesColumn.add(obj2.getId() + "");

			Data data = new Data(tpColumn, nmColumn, valuesColumn, "Disciplina");

			try {
				this.db.insertReg(data);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			break;
		case 3: {

			ArrayList<Integer> tpColumn = new ArrayList<Integer>();

			tpColumn.add(0);

			ArrayList<String> nmColumn = new ArrayList<String>();

			nmColumn.add("nmAluno");

			ArrayList<String> valuesColumn = new ArrayList<String>();

			valuesColumn.add(novoObjetoPadrao);

			Data data = new Data(tpColumn, nmColumn, valuesColumn, "Aluno");

			try {
				this.db.insertReg(data);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ArrayList<String> result = null;

			try {
				result = this.db.selectReg("Aluno", "MAX(idAluno)", "");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			String vetRet[] = result.get(0).split(";");
			int idAluno = Integer.parseInt(vetRet[0]);

			tpColumn.clear();
			nmColumn.clear();
			valuesColumn.clear();

			tpColumn.add(1);
			tpColumn.add(1);

			nmColumn.add("idAluno");
			nmColumn.add("idDisciplina");

			valuesColumn.add(idAluno + "");
			valuesColumn.add(obj2.getId() + "");

			Data data2 = new Data(tpColumn, nmColumn, valuesColumn,
					"DisciplinaAluno");

			try {
				this.db.insertReg(data2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			break;
		}
	}

	public void updateRegistro(ObjetoPadrao obj, String alteracao) {

		String tabela = "";
		String where = "";
		String column = "";

		switch (obj.getTipoObjeto()) {
		case 1: {

			tabela = "Curso";
			where = "idCurso = " + obj.getId();
			column = "nmCurso";
		}
			break;
		case 2: {

			tabela = "Disciplina";
			where = "idDisciplina = " + obj.getId();
			column = "nmDisciplina";
		}
			break;
		case 3: {

			tabela = "Aluno";
			where = "idAluno = " + obj.getId();
			column = "nmAluno";
		}
			break;
		}

		try {

			ArrayList<Integer> tpColumn = new ArrayList<Integer>();
			tpColumn.add(0);

			ArrayList<String> nmColumn = new ArrayList<String>();
			nmColumn.add(column);

			ArrayList<String> valueColumn = new ArrayList<String>();
			valueColumn.add(alteracao);

			Data data = new Data(tpColumn, nmColumn, valueColumn, tabela);
			this.db.updateReg(data, where);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
