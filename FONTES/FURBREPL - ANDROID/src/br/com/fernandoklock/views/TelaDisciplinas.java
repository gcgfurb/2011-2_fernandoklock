package br.com.fernandoklock.views;

import java.io.IOException;
import java.util.ArrayList;

import br.com.fernandoklock.ControlApplication.AtualizaDB;
import br.com.fernandoklock.ModelApplication.Curso;
import br.com.fernandoklock.ModelApplication.Disciplina;
import br.com.fernandoklock.replication.ParameterReplication;
import br.com.fernandoklock.replication.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class TelaDisciplinas extends Activity {

	private ListView listaDisciplinas;
	private TelaAlunos telaAlunos;
	private ArrayList<Disciplina> disciplinas;
	private Curso curso;
	private AtualizaDB db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.disciplinas);

		Intent intent = this.getIntent();
		curso = (Curso) intent.getSerializableExtra("Curso");

		ParameterReplication paramReplication = null;
		try {
			paramReplication = new ParameterReplication(this,
					"192.168.1.100", 15001, true, false, new byte[] { 0x00,
							0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
							0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f });
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			this.db = new AtualizaDB("FurbDB", this, paramReplication);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		openListView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.menu_disciplina, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {

		case R.id.add_disciplina: {
			Intent intent = new Intent(TelaDisciplinas.this, TelaGenerica.class);
			intent.putExtra("ObjetoPadraoFilho", new Disciplina(0, ""));
			intent.putExtra("ObjetoPadraoPai", this.curso);
			startActivity(intent);
		}
			break;
		case R.id.preferencias: {
			Intent intent = new Intent(TelaDisciplinas.this,
					TelaPreferencias.class);
			startActivity(intent);
		}
			break;
		}

		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		if (v.getId() == R.id.listaDisciplinas) {

			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			menu.setHeaderTitle(this.disciplinas.get(info.position).getNome());

			String[] menuItems = new String[] { "Alterar", "Excluir" };

			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}

		}

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		int menuItemIndex = item.getItemId();

		String[] menuItems = new String[] { "Alterar", "Excluir" };

		String menuItemName = menuItems[menuItemIndex];

		final Disciplina disciplina = this.disciplinas.get(info.position);
		String listItemName = disciplina.getNome();

		switch (menuItemIndex) {
		case 0: {
			Intent intent = new Intent(TelaDisciplinas.this, TelaGenerica.class);
			intent.putExtra("ObjetoPadraoFilho", disciplina);
			intent.putExtra("ObjetoPadraoPai", this.curso);
			startActivity(intent);
		}
			break;

		case 1: {
			

			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setMessage("Deseja excluir a disciplina " + disciplina.getNome() + " do sistema de chamada?");
			
			dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					deleteDisciplina(disciplina);	
				}
			});
			
			dialog.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					return;		
				}
			});
			
			dialog.setTitle("Aviso");
			dialog.show();
			
		}
			break;
		}

		openListView();

		return true;
	}

	
	@Override
	protected void onResume() {

		openListView();
		super.onResume();
	}

	public void openListView() {

		this.disciplinas = returnDisciplinas(curso);

		final String vetDisciplinas[] = new String[disciplinas.size()];

		int contDisciplinas = 0;

		for (Disciplina disc : disciplinas) {
			vetDisciplinas[contDisciplinas] = disc.getNome();
			contDisciplinas++;
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, vetDisciplinas);

		listaDisciplinas = (ListView) findViewById(R.id.listaDisciplinas);
		listaDisciplinas.setAdapter(adapter);

		listaDisciplinas.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView arg0, View arg1, int arg2,
					long arg3) {

				Disciplina disciplina = null;

				for (Disciplina disc : disciplinas) {
					if (disc.getNome().equals(
							vetDisciplinas[(int) listaDisciplinas
									.getItemIdAtPosition(arg2)])) {
						disciplina = disc;
						break;
					}
				}

				if (disciplina != null) {
					Intent intent = new Intent(TelaDisciplinas.this,
							TelaAlunos.class);
					intent.putExtra("Disciplina", disciplina);
					startActivity(intent);
				}
			}
		});

		registerForContextMenu(this.listaDisciplinas);
	}

	public void deleteDisciplina(Disciplina disciplina) {

		try {
			this.db.deleteReg("Disciplina",
					"idDisciplina = " + disciplina.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		openListView();

	}

	public ArrayList<Disciplina> returnDisciplinas(Curso curso) {

		ArrayList<String> resultSelect = new ArrayList<String>();
		ArrayList<Disciplina> disciplinas = new ArrayList<Disciplina>();

		try {
			resultSelect = this.db.selectReg("Curso, Disciplina",
					"Disciplina.idDisciplina,Disciplina.nmDisciplina",
					" where Curso.idCurso = " + curso.getId() + " and "
							+ "Disciplina.idCurso = Curso.idCurso");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (String s : resultSelect) {
			String split[] = s.split(";");
			disciplinas
					.add(new Disciplina(Integer.parseInt(split[0]), split[1]));
		}

		return disciplinas;
	}

}
