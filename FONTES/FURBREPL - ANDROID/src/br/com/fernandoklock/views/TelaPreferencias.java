package br.com.fernandoklock.views;

import java.io.IOException;
import java.util.ArrayList;

import com.google.android.c2dm.C2DMessaging;

import br.com.fernandoklock.ControlApplication.AtualizaDB;
import br.com.fernandoklock.database.Data;
import br.com.fernandoklock.replication.ParameterReplication;
import br.com.fernandoklock.replication.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

public class TelaPreferencias extends Activity {

	String ant_nmHost = "";
	int ant_nrPort = 0;
	boolean ant_chkCriptografia = false;
	boolean ant_chkTarget = false;
	ParameterReplication paramReplication = null;
	AtualizaDB db = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preferencias);

		final CheckBox chkCriptografia = (CheckBox) findViewById(R.id.criptografar);
		final CheckBox chkCripto128 = (CheckBox) findViewById(R.id.cripto128);
		final CheckBox chkCripto192 = (CheckBox) findViewById(R.id.cripto192);
		final CheckBox chkCripto256 = (CheckBox) findViewById(R.id.cripto256);
		final CheckBox chkTarget = (CheckBox) findViewById(R.id.tpReplicacao);
		final EditText nmHost = (EditText) findViewById(R.id.txtServidor);
		final EditText nrPort = (EditText) findViewById(R.id.txtPorta);

		Button btPreferencias = (Button) findViewById(R.id.btPreferencias);
		Button btConfRepl = (Button) findViewById(R.id.btConfRepl);
		Button btRegisC2DM = (Button) findViewById(R.id.btRegisC2DM);

		try {
			paramReplication = new ParameterReplication(this, "", 15001, true,
					true, new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
							0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d,
							0x0e, 0x0f });
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			db = new AtualizaDB("FurbDB", this, paramReplication);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		paramReplication.searchParameterReplication();

		nmHost.setText(paramReplication.getNmHost());
		ant_nmHost = paramReplication.getNmHost();
		nrPort.setText(paramReplication.getNrPortHost() + "");
		ant_nrPort = paramReplication.getNrPortHost();

		if (paramReplication.isPassiveClient()) {
			ant_chkTarget = true;
			chkTarget.setChecked(true);
		}

		if (paramReplication.isActiveCrypto()) {
			ant_chkCriptografia = true;
			chkCriptografia.setChecked(true);
			
			chkCripto128.setEnabled(true);
			chkCripto192.setEnabled(true);
			chkCripto256.setEnabled(true);
		}else{
			chkCripto128.setEnabled(false);
			chkCripto192.setEnabled(false);
			chkCripto256.setEnabled(false);
		}
		
		chkCripto128.setChecked(true);
		
		btPreferencias.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean criptografar = chkCriptografia.isChecked();
				boolean serverTarget = chkTarget.isChecked();
				String porta = nrPort.getText().toString();
				String host = nmHost.getText().toString();
				boolean replicou = replicar(criptografar, chkCripto128.isChecked(),chkCripto192.isChecked(), 
						  chkCripto256.isChecked(), serverTarget, porta,
						host);

				if (replicou)
					finish();
			}
		});

		btConfRepl.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean criptografar = chkCriptografia.isChecked();
				boolean serverTarget = chkTarget.isChecked();
				String porta = nrPort.getText().toString();
				String host = nmHost.getText().toString();
				atualizaConfg(criptografar, serverTarget, porta, host);
				finish();
			}
		});

		btRegisC2DM.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				registraC2DM();
				finish();
			}
		});

		chkCriptografia
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (!isChecked) {
							chkCripto128.setEnabled(false);
							chkCripto192.setEnabled(false);
							chkCripto256.setEnabled(false);
						} else {
							chkCripto128.setEnabled(true);
							chkCripto192.setEnabled(true);
							chkCripto256.setEnabled(true);
						}

					}
				});

		chkCripto128.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					chkCripto192.setChecked(false);
					chkCripto256.setChecked(false);
				}
			}
		});

		chkCripto192.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					chkCripto128.setChecked(false);
					chkCripto256.setChecked(false);
				}
			}
		});

		chkCripto256.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					chkCripto128.setChecked(false);
					chkCripto192.setChecked(false);
				}
			}
		});
	}

	public void atualizaConfg(boolean criptografar, boolean serverTarget,
			String porta, String host) {

		if ((ant_chkCriptografia != criptografar)
				|| (ant_chkTarget != serverTarget)
				|| (ant_nrPort != Integer.parseInt(porta))
				|| (!ant_nmHost.equals(host))) {

			ant_chkCriptografia = criptografar;
			ant_chkTarget = serverTarget;
			ant_nrPort = Integer.parseInt(porta);
			ant_nmHost = host;

			try {
				paramReplication.clearRegistryParameterReplication();
				paramReplication.addRegistryParameterReplication(host,
						Integer.parseInt(porta), serverTarget, criptografar);
				paramReplication.searchParameterReplication();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public boolean replicar(boolean criptografar,boolean cripto128,boolean cripto192, boolean cripto256,  boolean serverTarget,
			String porta, String host) {

		if ((ant_chkCriptografia != criptografar)
				|| (ant_chkTarget != serverTarget)
				|| (ant_nrPort != Integer.parseInt(porta))
				|| (!ant_nmHost.equals(host))) {

			ant_chkCriptografia = criptografar;
			ant_chkTarget = serverTarget;
			ant_nrPort = Integer.parseInt(porta);
			ant_nmHost = host;

			try {
				paramReplication.clearRegistryParameterReplication();
				paramReplication.addRegistryParameterReplication(host,
						Integer.parseInt(porta), serverTarget, criptografar);
				paramReplication.searchParameterReplication();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(cripto128){
			paramReplication.setKeyCrypto(new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
													   0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f});
			Log.e("CHAVE", "128");
		}else if(cripto192){
			paramReplication.setKeyCrypto(new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
													  0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
													  0x0f,0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07 });
			Log.e("CHAVE", "192");
		}else if(cripto256){
			paramReplication.setKeyCrypto(new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
													   0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
													   0x0f, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
													   0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07});
			Log.e("CHAVE", "256");
		}

		long time = System.currentTimeMillis();
		try {
			db.startReplication();
		} catch (Exception e) {
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setMessage("N�o foi possivel estabelecer a replica��o!");

			dialog.setNeutralButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							return;
						}
					});

			dialog.setTitle("Aviso");
			dialog.show();

			return false;
		}
		

		long time2 = System.currentTimeMillis();
		Log.e("TEMPO DA Replica��o", time2 - time + "");

		return true;
	}

	public void registraC2DM() {
		C2DMessaging.register(this, "klockfernando@gmail.com");
	}
}
