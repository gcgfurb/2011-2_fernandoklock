package br.com.fernandoklock.views;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import com.google.android.c2dm.C2DMessaging;

import br.com.fernandoklock.database.Data;
import br.com.fernandoklock.replication.C2DMReceiver;
import br.com.fernandoklock.replication.ParameterC2DM;
import br.com.fernandoklock.replication.ParameterReplication;
import br.com.fernandoklock.replication.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import br.com.fernandoklock.ControlApplication.AtualizaDB;
import br.com.fernandoklock.ModelApplication.*;

public class TelaCursos extends Activity  {

	private ListView listaCursos;
	private TelaDisciplinas telaDisciplinas;
	private ArrayList<Curso> arrayCursos;
	private AtualizaDB db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Set Layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cursos);	
		
		ParameterC2DM paramC2DM = null;
		
		try {
			paramC2DM = new ParameterC2DM(this, "klockfernando@gmail.com", 
					                      "Framework FURBRepl", "Mensagem do framework FURBRepl",
					                      br.com.fernandoklock.views.TelaPreferencias.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ParameterReplication paramReplication = null;
		
		try {
			paramReplication = new ParameterReplication(this,
					"192.168.1.100", 15001, true, false, new byte[] { 0x00,
							0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
							0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f });
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			this.db = new AtualizaDB("FurbDB", this, paramReplication);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		openListView(); 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.menu_curso, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {

		case R.id.add_curso: {
			Intent intent = new Intent(TelaCursos.this, TelaGenerica.class);
			intent.putExtra("ObjetoPadraoFilho", new Curso(0, ""));
			intent.putExtra("ObjetoPadraoPai", new Curso(0, ""));
			startActivity(intent);
		}
			break;

		case R.id.preferencias: {
			Intent intent = new Intent(TelaCursos.this, TelaPreferencias.class);
			startActivity(intent);
		}
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		if (v.getId() == R.id.listaCursos) {

			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			menu.setHeaderTitle(this.arrayCursos.get(info.position).getNome());

			String[] menuItems = new String[] { "Alterar", "Excluir" };

			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		int menuItemIndex = item.getItemId();

		String[] menuItems = new String[] { "Alterar", "Excluir" };

		String menuItemName = menuItems[menuItemIndex];

		final Curso curso = this.arrayCursos.get(info.position);
		String listItemName = curso.getNome();

		switch (menuItemIndex) {
		case 0: {
			Intent intent = new Intent(TelaCursos.this, TelaGenerica.class);
			intent.putExtra("ObjetoPadraoFilho", curso);
			intent.putExtra("ObjetoPadraoPai", curso);
			startActivity(intent);
		}
			break;

		case 1: {
			
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setMessage("Deseja excluir o curso " + curso.getNome() + " do sistema de chamada?");
			
			dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					deleteCurso(curso);	
				}
			});
			
			dialog.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					return;		
				}
			});
			
			dialog.setTitle("Aviso");
			dialog.show();
			
		}
			break;
		}

		openListView();
		
		return true;
	}

	@Override
	protected void onResume() {

		openListView(); 
		super.onResume();
	}

	public void openListView() {

		this.arrayCursos = returnCursos();

		final String vetCursos[] = new String[this.arrayCursos.size()];

		int contCursos = 0;

		for (Curso curso : this.arrayCursos) {
			vetCursos[contCursos] = curso.getNome();
			contCursos++;
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, vetCursos);
		listaCursos = (ListView) findViewById(R.id.listaCursos);
		listaCursos.setAdapter(adapter);

		listaCursos.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView arg0, View arg1, int arg2,
					long arg3) {

				Curso curso = null;

				for (Curso c : arrayCursos) {
					if (c.getNome().equals(
							vetCursos[(int) listaCursos
									.getItemIdAtPosition(arg2)])) {
						curso = c;
						break;
					}
				}

				if (curso != null) {
					Intent intent = new Intent(TelaCursos.this,
							TelaDisciplinas.class);
					intent.putExtra("Curso", curso);
					startActivity(intent);
				}
			}
		});
		registerForContextMenu(this.listaCursos);
	}

	public void deleteCurso(Curso curso) {

		try {
			this.db.deleteReg("Curso", "idCurso = " + curso.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		openListView();
	}

	public ArrayList<Curso> returnCursos() {

		ArrayList<String> resultSelect = new ArrayList<String>();
		ArrayList<Curso> cursos = new ArrayList<Curso>();

		try {
			resultSelect = this.db.selectReg("Curso", "idCurso,nmCurso", "");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (String s : resultSelect) {
			String split[] = s.split(";");
			cursos.add(new Curso(Integer.parseInt(split[0]), split[1]));
		}

		return cursos;
	}

}
