package br.com.fernandoklock.ModelApplication;

public interface ObjetoPadrao {

	public String getNome();
	public int getId();
	public int getTipoObjeto();
	
}
