package br.com.fernandoklock.ModelApplication;

import java.io.Serializable;

import android.location.GpsStatus.NmeaListener;

public class Aluno implements Serializable, ObjetoPadrao {
	
	private String nome;
	private int ID;
	
	public Aluno( int ID, String nome) {
		this.nome = nome;
		this.ID = ID;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setID(int iD) {
		ID = iD;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}

	@Override
	public int getTipoObjeto() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return ID;
	}

}
