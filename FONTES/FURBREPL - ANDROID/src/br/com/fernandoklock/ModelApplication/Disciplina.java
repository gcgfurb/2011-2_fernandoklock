package br.com.fernandoklock.ModelApplication;

import java.io.Serializable;

public class Disciplina implements Serializable, ObjetoPadrao {
	
	private int idDisciplina;
	private String nmDisciplina;
	
	public Disciplina(int idDisciplina, String nmDisciplina) {
		this.idDisciplina = idDisciplina;
		this.nmDisciplina = nmDisciplina;
	}
	
	public void setIdDisciplina(int idDisciplina) {
		this.idDisciplina = idDisciplina;
	}
	
	public void setNmDisciplina(String nmDisciplina) {
		this.nmDisciplina = nmDisciplina;
	}
	
	@Override
	public String toString() {
		return nmDisciplina;
	}

	@Override
	public String getNome() {
		return this.nmDisciplina;
	}

	@Override
	public int getTipoObjeto() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return idDisciplina;
	}
}
