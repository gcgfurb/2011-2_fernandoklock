package br.com.fernandoklock.ModelApplication;

import java.io.Serializable;

public class Curso implements Serializable, ObjetoPadrao {
	
	private int idCurso;
	private String nmCurso;
	
	public Curso(int idCurso, String nmCurso) {
		this.idCurso = idCurso;
		this.nmCurso = nmCurso;
	}

	public void setIdCurso(int idCurso) {
		this.idCurso = idCurso;
	}
	
	public void setNmCurso(String nmCurso) {
		this.nmCurso = nmCurso;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return nmCurso;
	}

	@Override
	public String getNome() {
		return this.nmCurso;
	}

	@Override
	public int getTipoObjeto() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return idCurso;
	}
	
}
