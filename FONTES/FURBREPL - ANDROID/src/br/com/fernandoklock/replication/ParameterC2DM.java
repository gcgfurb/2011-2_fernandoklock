package br.com.fernandoklock.replication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ParameterC2DM extends SQLiteOpenHelper implements Serializable {

	private String email;
	private String titleMessageReceive;
	private String nameMessageReceive;
	private Class notificationClass;
	private Context context;
	private SQLiteDatabase dbReplication;

	private static String dbName = "ReplicationDB";
	private static String dbPath = "/data/data/br.com.fernandoklock.replication/databases/";

	// inicia e alimenta a base de dados com os parametros C2DM
	public ParameterC2DM(Context context, String email,
			String titleMessageReceive, String nameMessageReceive,
			Class notificationClass) throws IOException {

		super(context, "ReplicationDB", null, 33);

		this.context = context;
		this.email = email;
		this.titleMessageReceive = titleMessageReceive;
		this.nameMessageReceive = nameMessageReceive;
		this.notificationClass = notificationClass;

		createDataBase();

		try {
			clearRegistryC2DMParameter();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("ReplicationDB",
					"Erro ao limpar registros da tabela ParameterC2DM, verifique o banco DBReplication");
		}

		try {
			addRegistryC2DMParameter(email, notificationClass.getName(),
					titleMessageReceive, nameMessageReceive);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("ReplicationDB",
					"Erro ao inserir registros na tabela ParameterC2DM, verifique o banco DBReplication");
		}
	}

	// construtor para fazer busca dos parametros C2DM
	public ParameterC2DM(Context context) throws IOException {
		super(context, "ReplicationDB", null, 33);
		this.context = context;
		createDataBase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stu
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();

		if (dbExist) {
			close();
		} else {

			// By calling this method and empty database will be created into
			// the default system path
			// of your application so we are gonna be able to overwrite that
			// database with our database.
			this.getReadableDatabase();

			try {

				copyDataBase();

			} catch (IOException e) {

				throw new Error("Error copying database");

			}

			this.open();
		}

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {

		this.dbReplication = null;

		try {
			
			if(!databaseExist())
				return false;
				
			String myPath = this.dbPath + this.dbName;
			this.dbReplication = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);

		} catch (Exception e) {

			// database does't exist yet.
		}

		return this.dbReplication != null ? true : false;

	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = context.getAssets().open(this.dbName);

		// Path to the just created empty db
		String outFileName = this.dbPath + this.dbName;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}
	
	private boolean databaseExist(){
		File dbFile = new File(dbPath + dbName);
		return dbFile.exists();
	}

	public String getEmail() {
		return email;
	}

	public Class getNotificationClass() {
		return notificationClass;
	}

	public String getNameMessageReceive() {
		return nameMessageReceive;
	}

	public String getTitleMessageReceive() {
		return titleMessageReceive;
	}

	public void addRegistryC2DMParameter(String dsEmail, String nmApplication,
			String dsTitleC2DM, String dsMessageC2DM) throws Exception {
		
		open();

		ContentValues cv = null;

		cv = new ContentValues();
		cv.put("dsEmail", dsEmail);
		cv.put("nmApplication", nmApplication);
		cv.put("dsTitleC2DM", dsTitleC2DM);
		cv.put("dsMessageC2DM", dsMessageC2DM);

		long returnInsert = dbReplication.insert("ParameterC2DM",
				"idParameter", cv);

		if (returnInsert == -1) {
			Log.e("Erro Insert - ReplicationDB",
					"Nao foi possivel inserir o registro da tabela ParameterC2DM");
		}
		
		close();
	}

	public void clearRegistryC2DMParameter() throws Exception {
		open();
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete("ParameterC2DM", "", new String[] {});
		db.close();
		close();
	}

	public ArrayList<String> searchRegistryParameterC2DM() {

		open();
		
		ArrayList<String> result = null;

		int idParametro = 0;

		Cursor cur = dbReplication.rawQuery(
				"Select MAX(idParameter) from ParameterC2DM", null);

		if (!cur.moveToFirst()) {
			cur.close();
			close();
			return null;
		}

		int rows = cur.getCount();

		for (int row = 0; row < rows; row++) {

			if (!cur.getString(0).contains("null")) {
				idParametro = Integer.parseInt(cur.getString(0));
			}
			if (!cur.moveToNext()) {
				break;
			}
		}

		cur.close();

		if (idParametro == 0) {

			close();
			return null;

		} else {

			Cursor cur_find = dbReplication
					.rawQuery(
							"Select dsEmail, nmApplication, dsTitleC2DM, dsMessageC2DM from ParameterC2DM where idParameter = "
									+ idParametro, null);

			if (!cur_find.moveToFirst()) {
				cur_find.close();
				close();
				return null;
			}

			if (!cur_find.getString(0).contains("null")) {

				result = new ArrayList<String>();

				result.add(cur_find.getString(0));
				result.add(cur_find.getString(1));
				result.add(cur_find.getString(2));
				result.add(cur_find.getString(3));
			}

			cur_find.close();
			close();
		}

		return result;
	}

	@Override
	public synchronized void close() {
		if (this.dbReplication == null)
			return;

		this.dbReplication.close();
		this.dbReplication = null;
	}

	public synchronized void open() {
		this.dbReplication = context.openOrCreateDatabase(dbName,
				context.MODE_PRIVATE, null);
	}

	public synchronized boolean isOpen() {

		if (this.dbReplication == null)
			return false;

		return true;
	}

}
