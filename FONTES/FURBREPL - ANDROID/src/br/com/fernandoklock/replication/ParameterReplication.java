package br.com.fernandoklock.replication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ParameterReplication extends SQLiteOpenHelper implements
		Serializable {

	private String nmHost;
	private int nrPortHost;

	private boolean activeCrypto;
	private boolean passiveClient;
	private byte[] keyCrypto;

	private Context context;
	private SQLiteDatabase dbReplication;

	private static String dbName = "ReplicationDB";
	private static String dbPath = "/data/data/br.com.fernandoklock.replication/databases/";

	public ParameterReplication(Context context, String nmHost, int nrPortHost,
			boolean activeCrypto, boolean passiveClient, byte[] keyCrypto)
			throws IOException {

		super(context, dbName, null, 33);

		this.nmHost = nmHost;
		this.nrPortHost = nrPortHost;
		this.activeCrypto = activeCrypto;
		this.passiveClient = passiveClient;
		this.keyCrypto = keyCrypto;
		this.context = context;

		createDataBase();
		searchParameterReplication();
	}

	public ParameterReplication(Context context) throws IOException {
		super(context, dbName, null, 33);
		this.context = context;

		createDataBase();
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();

		if (dbExist) {
			close();
		} else {

			// By calling this method and empty database will be created into
			// the default system path
			// of your application so we are gonna be able to overwrite that
			// database with our database.
			this.getReadableDatabase();

			try {

				copyDataBase();

			} catch (IOException e) {

				throw new Error("Error copying database");

			}
		}

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {

		this.dbReplication = null;

		try {

			if (!databaseExist())
				return false;

			String myPath = this.dbPath + this.dbName;
			this.dbReplication = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);

		} catch (Exception e) {
			// database does't exist yet.
		}

		return this.dbReplication != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = context.getAssets().open(this.dbName);

		// Path to the just created empty db
		String outFileName = this.dbPath + this.dbName;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	private boolean databaseExist() {
		File dbFile = new File(dbPath + dbName);
		return dbFile.exists();
	}

	public String getNmHost() {
		return nmHost;
	}

	public int getNrPortHost() {
		return nrPortHost;
	}

	public boolean isActiveCrypto() {
		return activeCrypto;
	}

	public boolean isPassiveClient() {
		return passiveClient;
	}

	public void setActiveCrypto(boolean activeCrypto) {
		this.activeCrypto = activeCrypto;
	}

	public void setPassiveClient(boolean passiveClient) {
		this.passiveClient = passiveClient;
	}

	public byte[] getKeyCrypto() {
		return keyCrypto;
	}

	public void setKeyCrypto(byte[] keyCrypto) {
		this.keyCrypto = keyCrypto;
	}

	public void clearRegistryParameterReplication() throws Exception {
		open();
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete("ParameterReplication", "", new String[] {});
		db.close();
		close();
	}

	public void addRegistryParameterReplication(String nmHost, int nrPort,
			boolean passiveClient, boolean crypto) throws Exception {

		open();

		ContentValues cv = null;

		cv = new ContentValues();
		cv.put("nmHost", nmHost);
		cv.put("nrPort", nrPort);

		if (passiveClient) {
			cv.put("flgReplication", "1");
		} else {
			cv.put("flgReplication", "0");
		}

		if (crypto) {
			cv.put("flgCrypto", "1");
		} else {
			cv.put("flgCrypto", "0");
		}

		long returnInsert = dbReplication.insert("ParameterReplication",
				"idParameter", cv);

		if (returnInsert == -1) {
			Log.e("Erro Insert - ReplicationDB",
					"Nao foi possivel inserir o registro da tabela ParameterReplication");
		}

		close();
	}

	public void searchParameterReplication() {

		open();

		ArrayList<String> result = null;

		int idParametro = 0;

		Cursor cur = dbReplication.rawQuery(
				"Select MAX(idParameter) from ParameterReplication", null);

		if (!cur.moveToFirst()) {
			cur.close();
			close();
			return;
		}

		int rows = cur.getCount();

		for (int row = 0; row < rows; row++) {
			if (cur.getInt(0) != 0) {
				idParametro = cur.getInt(0);
			}
			if (!cur.moveToNext()) {
				break;
			}
		}

		cur.close();

		if (idParametro == 0) {

			close();
			return;

		} else {

			Cursor cur_find = dbReplication
					.rawQuery(
							"Select nmHost, nrPort, flgReplication, flgCrypto  from ParameterReplication where idParameter = "
									+ idParametro, null);

			if (!cur_find.moveToFirst()) {
				cur_find.close();
				close();
				return;
			}

			if (!cur_find.getString(0).contains("null")) {
				this.nmHost = cur_find.getString(0);
				this.nrPortHost = Integer.parseInt(cur_find.getString(1));

				if (cur_find.getString(2).equals("1")) {
					this.passiveClient = true;
				} else {
					this.passiveClient = false;
				}

				if (cur_find.getString(3).equals("1")) {
					this.activeCrypto = true;
				} else {
					this.activeCrypto = false;
				}
			}

			cur_find.close();
			close();
		}
	}

	public String tamanhoChave() {

		int tamVet = this.keyCrypto.length;

		if (!activeCrypto) {
			return "0";
		} else if (tamVet == 16) {
			return "128";
		} else if (tamVet == 24) {
			return "192";
		} else if (tamVet == 32) {
			return "256";
		}

		return "0";
	}

	@Override
	public synchronized void close() {
		if (this.dbReplication == null)
			return;

		this.dbReplication.close();
		this.dbReplication = null;
	}

	public synchronized void open() {
		this.dbReplication = context.openOrCreateDatabase(dbName,
				context.MODE_PRIVATE, null);
	}

	public synchronized boolean isOpen() {

		if (this.dbReplication == null)
			return false;

		return true;
	}

}
