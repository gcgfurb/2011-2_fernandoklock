package br.com.fernandoklock.replication;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class SocketClient {

	private ArrayList<String> dadosSync;
	private ManagerReplication replManager;
	private ParameterReplication paramReplication;

	public SocketClient(ManagerReplication replManager,
			ParameterReplication paramReplication) {
		this.replManager = replManager;
		this.paramReplication = paramReplication;
	}

	public SocketClient(ParameterReplication paramReplication) {
		this.paramReplication = paramReplication;
	}

	public void enviaDados() throws Exception {

		Socket smtpSocket = null;
		DataOutputStream os = null;
		DataInputStream is = null;

		InetAddress addr = InetAddress.getByName(this.paramReplication
				.getNmHost());
		SocketAddress sockaddr = new InetSocketAddress(addr,
				this.paramReplication.getNrPortHost());
		smtpSocket = new Socket();
		smtpSocket.connect(sockaddr, 2000);

		os = new DataOutputStream(smtpSocket.getOutputStream());
		is = new DataInputStream(smtpSocket.getInputStream());

		if (smtpSocket != null && os != null && is != null) {

			ArrayList<String> dadosLidos = new ArrayList<String>();
			boolean errorReplication = false;

			String dadoLido = "";
			
			os.writeUTF(this.paramReplication.tamanhoChave());

			dadoLido = is.readUTF();

			if (dadoLido.equals("NOK")) {
				throw new Exception("Chaves de criptografia incompativeis!");
			}

			if (!this.paramReplication.isPassiveClient()) {

				dadosSync = this.replManager.retDataSync();

				os.writeUTF("BeginReplicationSource");
				dadoLido = is.readUTF();

				if (dadoLido.equals("NOK")) {
					throw new Exception(
							"Server e Client de replicacao est�o configurados como SOURCE!");
				}

				for (String dadoSync : this.dadosSync) {
					os.writeUTF(dadoSync);
				}

				os.writeUTF("EndOfTransmission");

				do {
					dadoLido = is.readUTF();

					if (dadoLido.equals("EndOfTransmission")) {
						break;
					} else if (dadoLido.equals("BeginOfTransmissionError")) {
						dadosLidos.clear();
					} else if (dadoLido.equals("EndOfTransmissionError")) {
						errorReplication = true;
						break;
					} else {
						dadosLidos.add(dadoLido);
					}
				} while (true);
				// Atualiza o banco DBReplication com a situa��o ocorrida
				this.replManager
						.returnReplication(errorReplication, dadosLidos);

			} else {

				os.writeUTF("BeginReplicationTarget");

				dadoLido = is.readUTF();

				if (dadoLido.equals("NOK")) {
					throw new Exception(
							"Server e Client de replicacao est�o configurados como TARGET!");
				}

				do {
					dadoLido = is.readUTF();

					if (dadoLido.equals("EndOfTransmission")) {
						break;
					}

					dadosLidos.add(dadoLido);

				} while (true);

				// APLICA REGISTROS
				ArrayList<String> errorAddTarget = this.replManager
						.addRegistryBaseTarget(dadosLidos);

				int errors = errorAddTarget.size();

				// VERIFICAR SE HOUVE ERRO AO INSERIR OS REGISTROS
				if (errors > 0) {
					os.writeUTF("BeginOfTransmissionError");

					for (String errorDB : errorAddTarget) {
						os.writeUTF(errorDB);
					}

					os.writeUTF("EndOfTransmissionError");

				} else {
					os.writeUTF("EndOfTransmission");
				}
			}

			if (dadosSync != null) {
				dadosSync.clear();
			}

			os.close();
			is.close();
			smtpSocket.close();

		}
	}

	public void registraC2DMServer(String registrationid)
			throws UnknownHostException, IOException {

		Socket smtpSocket = null;
		DataOutputStream os = null;
		DataInputStream is = null;

		smtpSocket = new Socket(this.paramReplication.getNmHost(),
				this.paramReplication.getNrPortHost());

		os = new DataOutputStream(smtpSocket.getOutputStream());
		is = new DataInputStream(smtpSocket.getInputStream());

		if (smtpSocket != null && os != null && is != null) {

			os.writeUTF("C2DMRegister");
			os.writeUTF(registrationid);

		}

		os.close();
		is.close();
		smtpSocket.close();

	}

	public ArrayList<String> getDadosSync() {
		return dadosSync;
	}

	public void setDadosSync(ArrayList<String> dadosSync) {
		this.dadosSync = dadosSync;
	}

}
