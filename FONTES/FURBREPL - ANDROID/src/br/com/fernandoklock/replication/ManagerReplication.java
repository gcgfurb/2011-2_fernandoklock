package br.com.fernandoklock.replication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import br.com.fernandoklock.crypto.CryptographyAES;
import br.com.fernandoklock.database.DBManipulate;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ManagerReplication extends SQLiteOpenHelper implements
		Serializable {

	private ParameterReplication paramReplication;
	private DBManipulate dbManipulate;

	private CryptographyAES crypto;
	private SocketClient clientSocket;

	private ArrayList<String> dadosSync;
	private SQLiteDatabase dbReplication = null;

	private Context context;

	private static String dbName = "ReplicationDB";
	private static String dbPath = "/data/data/br.com.fernandoklock.replication/databases/";

	public ManagerReplication(Context context,
			ParameterReplication paramReplication, DBManipulate dbManipulate)
			throws IOException {

		super(context, dbName, null, 33);

		this.paramReplication = paramReplication;
		this.dbManipulate = dbManipulate;
		this.context = context;

		if (this.paramReplication == null) {
			Log.e("ParameterReplication",
					"Objeto ParamterReplication nao encontrado");
			return;
		}

		if (this.dbManipulate == null) {
			Log.e("DBManipulate", "Objeto DBManipulate nao encontrado");
			return;
		}

		createDataBase();

		this.crypto = new CryptographyAES(this.paramReplication.getKeyCrypto());

		this.clientSocket = new SocketClient(this, this.paramReplication);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();

		if (dbExist) {

		} else {

			// By calling this method and empty database will be created into
			// the default system path
			// of your application so we are gonna be able to overwrite that
			// database with our database.
			this.getReadableDatabase();

			try {

				copyDataBase();

			} catch (IOException e) {

				throw new Error("Error copying database");

			}
		}

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {

		this.dbReplication = null;

		try {
			
			if(!databaseExist())
				return false;

			String myPath = this.dbPath + this.dbName;
			this.dbReplication = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);

		} catch (Exception e) {

			// database does't exist yet.
		}

		return this.dbReplication != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	public void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = context.getAssets().open(this.dbName);

		// Path to the just created empty db
		String outFileName = this.dbPath + this.dbName;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}
	
	private boolean databaseExist(){
		File dbFile = new File(dbPath + dbName);
		return dbFile.exists();
	}

	public void startReplication() throws Exception {
		clientSocket.enviaDados();
	}

	public ArrayList<String> retDataSync() {
		
		//atualiza chave da criptografia
		this.crypto.setChave(this.paramReplication.getKeyCrypto());
		
		ArrayList<String> dadosCryptoSync = new ArrayList<String>();
		this.dadosSync = retRegistryReplication();

		if (this.paramReplication.isActiveCrypto()) {
			
			// Criptografa as mensagens;
			for (String dados : this.dadosSync) {

				if (paramReplication.isActiveCrypto()) {
					
					try {
						dadosCryptoSync.add(this.crypto.cifrar(dados));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				} else {
					
					dadosCryptoSync.add(dados);
					
				}

			}
			
			return dadosCryptoSync;

		}

		return this.dadosSync;

	}

	public void addRegistryReplication(String registry) throws Exception {

		if (this.paramReplication.isPassiveClient())
			return;

		ContentValues cv = null;

		cv = new ContentValues();
		cv.put("dsRegistry", registry);
		cv.put("flgSync", "0");

		long returnInsert = dbReplication.insert("Registry", "idRegistry", cv);

		if (returnInsert == -1) {
			Log.e("Erro Insert - ReplicationDB",
					"Nao foi possivel inserir o registro da replicacao");
		}
	}

	private ArrayList<String> retRegistryReplication() {

		// Inicia Array
		ArrayList<String> result = new ArrayList<String>();

		Cursor cur = dbReplication
				.rawQuery(
						"Select idRegistry, dsRegistry from Registry where flgSync = 0",
						null);

		if (!cur.moveToFirst()) {
			cur.close();
			return result;
		}

		int rows = cur.getCount();

		for (int row = 0; row < rows; row++) {
			result.add(cur.getString(0) + ";" + cur.getString(1));

			if (!cur.moveToNext()) {
				cur.close();
				return result;
			}
		}

		cur.close();

		return result;
	}

	// Atualiza registros informando que o mesmo apresentou erro ao ser
	// executado na base de destino
	private void updateRegistryError(int idRegistry) {

		ContentValues cv = null;

		cv = new ContentValues();
		cv.put("flgSync", "2");

		this.dbReplication.update("Registry", cv, "idRegistry = " + idRegistry,
				new String[] {});
	}

	// Retorna os registros que apresentaram erros ao serem inseridos na base de
	// destino
	public ArrayList<String> errorRegistryReplication() {

		// Inicia Array
		ArrayList<String> result = new ArrayList<String>();

		Cursor cur = dbReplication.rawQuery(
				"Select dsRegistry from Registry Where Registry.flgSync = 2",
				null);

		if (!cur.moveToFirst()) {
			cur.close();
			return result;
		}

		int rows = cur.getCount();

		for (int row = 0; row < rows; row++) {
			result.add(cur.getString(0));

			if (!cur.moveToNext()) {
				cur.close();
				return result;
			}
		}

		cur.close();
		return result;
	}

	// Limpa registros que j� foram enviados para a base de destino
	private void clearRegistryReplicationSended() {

		SQLiteDatabase db = this.getWritableDatabase();
		db.delete("Registry", "flgSync = 0", new String[] {});
		db.close();

	}

	// Limpa os registros da base de replica��o que contem erros
	public void clearRegistryReplicationError() {

		SQLiteDatabase db = this.getWritableDatabase();
		db.delete("Registry", "flgSync = 2", new String[] {});
		db.close();

	}

	// Campo Registry.flgSync --- 0 = nao enviado --- 1 = enviado --- 2 = error
	public void returnReplication(boolean errorReplication,
			ArrayList<String> regErrorReplication) {

		//atualiza chave da criptografia
		this.crypto.setChave(this.paramReplication.getKeyCrypto());
		
		if (errorReplication) {
			// Se ocorreu erro, assinalar quais registros apresentaram problemas
			for (String regError : regErrorReplication) {

				String regDecrypt = "";

				if (paramReplication.isActiveCrypto()) {
					
					try {
						regDecrypt = this.crypto.decifrar(regError);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				} else {
					
					regDecrypt = regError;
					
				}

				String vet[] = regDecrypt.split(";");

				if (vet.length < 2) {
					continue;
				}

				try {
					updateRegistryError(Integer.parseInt(vet[0]));
				} catch (Exception e) {
					Log.e("ManagerReplication",
							"Class ManagerReplication - Erro no ao executar m�todo updateRegistryError ");
				}
			}
		}

		// Limpa os registros que nao apresentaram error
		clearRegistryReplicationSended();
	}

	// Aplica os registros que vieram na replica��o
	public ArrayList<String> addRegistryBaseTarget(ArrayList<String> regsTarget) {

		//atualiza chave da criptografia
		this.crypto.setChave(this.paramReplication.getKeyCrypto());
		
		ArrayList<String> errorAdd = new ArrayList<String>();

		for (String regTarget : regsTarget) {

			String regDecrypt = "";

			if (paramReplication.isActiveCrypto()) {
				
				try {
					regDecrypt = this.crypto.decifrar(regTarget);
				} catch (Exception e) {
					errorAdd.add(regTarget);
					continue;
				}
				
			} else {
				
				regDecrypt = regTarget;
			
			}
			
			// Se o cliente estiver enviado criptografado e o servidor n�o, ocorrera erro no split
			// Retornar erro de replicacao
			
			if(!Pattern.compile("update", Pattern.CASE_INSENSITIVE).matcher(regDecrypt).find() &&
			   !Pattern.compile("delete", Pattern.CASE_INSENSITIVE).matcher(regDecrypt).find() &&
			   !Pattern.compile("insert", Pattern.CASE_INSENSITIVE).matcher(regDecrypt).find()   ){
				errorAdd.add(regTarget);
				continue;
			}

			String[] vetDecrypt = regDecrypt.split(";");

			if (vetDecrypt.length < 2) {
				errorAdd.add(regTarget);
			}

			try {
				int res = applyBaseTarget(vetDecrypt[1]);

				if (res == 0) {
					errorAdd.add(regTarget);
				}
				// this.dbManipulate.getDb().execSQL(vetDecrypt[1]);
			} catch (Exception e) {
				errorAdd.add(regTarget);
			}
		}

		return errorAdd;
	}

	public void open(Context context) {
		this.dbReplication = context.openOrCreateDatabase(dbName,
				Context.MODE_PRIVATE, null);
	}


	public synchronized boolean isOpen() {

		if (this.dbReplication != null)
			return false;

		return true;
	}
	
	@Override
	public synchronized void close() {
		if (this.dbReplication == null)
			return;

		this.dbReplication.close();
		this.dbReplication = null;
	}


	public SQLiteDatabase getDbReplication() {
		return dbReplication;
	}

	public void setDbReplication(SQLiteDatabase dbReplication) {
		this.dbReplication = dbReplication;
	}

	private int applyBaseTarget(String sql) {

		sql = sql.replaceAll("'", "");
		sql = sql.replaceAll("\"", "");
		int result = 0;

		if (Pattern.compile("delete", Pattern.CASE_INSENSITIVE).matcher(sql)
				.find()) {

			StringTokenizer tokens = new StringTokenizer(sql);

			boolean clausula_where = false;
			String tabela = "";
			String where = "";

			while (tokens.hasMoreTokens()) {

				String token = tokens.nextToken();

				if (Pattern.compile("delete", Pattern.CASE_INSENSITIVE)
						.matcher(token).find()) {
					continue;
				}

				if (Pattern.compile("where", Pattern.CASE_INSENSITIVE)
						.matcher(token).find()) {
					clausula_where = true;
					continue;
				}

				if (clausula_where) {
					where += " " + token;
					continue;
				}

				tabela = token;
			}

			result = this.dbManipulate.getDb().delete(tabela, where,
					new String[] {});

		} else if (Pattern.compile("insert", Pattern.CASE_INSENSITIVE)
				.matcher(sql).find()) {

			StringTokenizer tokens = new StringTokenizer(sql);

			boolean clausula_values = false;
			boolean ant_into = false;
			String tabela = "";
			String columns_name = "";
			String values = "";

			while (tokens.hasMoreTokens()) {

				String token = tokens.nextToken();

				if (Pattern.compile("insert", Pattern.CASE_INSENSITIVE)
						.matcher(token).find()) {
					continue;
				}

				if (token.contains("(") || token.contains(")")) {
					continue;
				}

				if (ant_into) {
					tabela = token;
					ant_into = false;
					continue;
				}

				if (Pattern.compile("into", Pattern.CASE_INSENSITIVE)
						.matcher(token).find()) {
					ant_into = true;
					continue;
				}

				if (Pattern.compile("values", Pattern.CASE_INSENSITIVE)
						.matcher(token).find()) {
					clausula_values = true;
					continue;
				}

				if (clausula_values) {
					values += " " + token;
					continue;
				} else {
					columns_name += " " + token;
					continue;
				}
			}

			boolean column_default = true;

			ContentValues cv = new ContentValues();

			String vet_column_name[] = columns_name.split(",");
			String vet_column_values[] = values.split(",");
			String name_column_insert = "";

			for (int i = 0; i < vet_column_name.length; i++) {

				if (column_default) {
					name_column_insert = vet_column_name[i].trim();
				}

				cv.put(vet_column_name[i].trim(), vet_column_values[i].trim());
			}

			result = (int) this.dbManipulate.getDb().insert(tabela,
					name_column_insert, cv);

		} else if (Pattern.compile("update", Pattern.CASE_INSENSITIVE)
				.matcher(sql).find()) {

			StringTokenizer tokens = new StringTokenizer(sql);

			boolean clausula_where = false;
			boolean clausula_set = false;
			String tabela = "";
			String set = "";
			String where = "";

			while (tokens.hasMoreTokens()) {

				String token = tokens.nextToken();

				if (Pattern.compile("update", Pattern.CASE_INSENSITIVE)
						.matcher(token).find()) {
					continue;
				}

				if (Pattern.compile("set", Pattern.CASE_INSENSITIVE)
						.matcher(token).find()) {
					clausula_set = true;
					continue;
				}

				if (Pattern.compile("where", Pattern.CASE_INSENSITIVE)
						.matcher(token).find()) {
					clausula_where = true;
					clausula_set = false;
					continue;
				}

				if (clausula_set) {
					set += " " + token;
					continue;
				}

				if (clausula_where) {
					where += " " + token;
					continue;
				}

				tabela = token;
			}

			ContentValues cv = new ContentValues();

			String vet_column_alter[] = set.split(",");

			for (int i = 0; i < vet_column_alter.length; i++) {
				String vet_update[] = vet_column_alter[i].split("=");
				cv.put(vet_update[0].trim(), vet_update[1].trim());
			}

			result = this.dbManipulate.getDb().update(tabela, cv, where,
					new String[] {});

		}

		return result;
	}
}
