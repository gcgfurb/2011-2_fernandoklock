package br.com.fernandoklock.replication;

import java.io.IOException;
import java.util.ArrayList;

import com.google.android.c2dm.C2DMBaseReceiver;

import br.com.fernandoklock.replication.R;
import br.com.fernandoklock.replication.R.drawable;
import br.com.fernandoklock.replication.R.string;
import br.com.fernandoklock.views.Principal;
import br.com.fernandoklock.views.TelaPreferencias;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;

public class C2DMReceiver extends C2DMBaseReceiver {

	private ParameterC2DM paramC2DM;
	private String nmAplication;
	private String dsTitleC2DM;
	private String dsMessageC2DM;

	public C2DMReceiver() {
		// Email address currently not used by the C2DM Messaging framework
		super("klockfernando@gmail.com");
	}

	@Override
	public void onRegistered(Context context, String registrationId)
			throws java.io.IOException {
		// The registrationId should be send to your applicatioin server.
		// We just log it to the LogCat view
		// We will copy it from there
		Log.e("C2DM", "Registration ID arrived: Fantastic!!!");
		Log.e("C2DM", registrationId);

		ParameterReplication paramReplication = new ParameterReplication(
				context);
		
		paramReplication.searchParameterReplication();

		SocketClient socket = new SocketClient(paramReplication);

		socket.registraC2DMServer(registrationId);
	};

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.e("C2DM", "Message: Fantastic!!!");

		try {
			this.paramC2DM = new ParameterC2DM(context);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ArrayList<String> result = this.paramC2DM.searchRegistryParameterC2DM();

		// Parameter C2DM -> dsEmail, nmAplication, dsTitleC2DM, dsMessageC2DM
		this.nmAplication = result.get(1);
		this.dsTitleC2DM = result.get(2);
		this.dsMessageC2DM = result.get(3);

		// Extract the payload from the message
		Bundle extras = intent.getExtras();
		if (extras != null) {
			System.out.println(extras.get("payload"));
			// Now do something smart based on the information
		}
		// Verifica se recebeu notifica��o para registro sen�o � uma notifica��o
		// simples de informa��o.
		if (intent.getAction().equals(
				"com.google.android.c2dm.intent.REGISTRATION")) {
			criaNotificacao(context, intent.getStringExtra("payload"));
		} else if (intent.getAction().equals(
				"com.google.android.c2dm.intent.RECEIVE")) {
			criaNotificacao(context, intent.getStringExtra("payload"));
		}
		// criaNotificacao(context, "Replicacao de dados!");
	}

	@Override
	public void onError(Context context, String errorId) {
		Log.e("C2DM", "Error occured!!!");
	}

	@Override
	public void onUnregistered(Context context) {

	}

	private void criaNotificacao(Context ctx, String evento) {

		// Recupera o servi�o do NotificationManager
		NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notificacao = new Notification(R.drawable.icon, this.dsTitleC2DM, System.currentTimeMillis());
		// Flag que vibra e emite um sinal sonoro at� o usu�rio clicar na
		// notifica��o
		notificacao.flags |= Notification.FLAG_INSISTENT;
		// Flag utilizada para remover a notifica��o da toolbar quando usu�rio
		// tiver clicado nela.
		notificacao.flags |= Notification.FLAG_AUTO_CANCEL;
		// PendingIntent para executar a Activity se o usu�rio selecionar a
		// notific�o
		Class classe = null;
		
		try {
			classe = Class.forName(this.nmAplication);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		PendingIntent p = PendingIntent.getActivity(ctx, 0, new Intent(ctx.getApplicationContext(),classe), 0);
		// Informac�es
		notificacao.setLatestEventInfo(ctx, this.dsTitleC2DM, this.dsMessageC2DM + evento, p);
		// Espera 100ms e vibra por 1000ms, depois espera por 1000 ms e vibra
		// por 1000ms.
		notificacao.vibrate = new long[] { 100, 1000, 1000, 1000 };
		// Id que identifica esta notifica��o
		notificationManager.notify(R.string.app_name, notificacao);

	}

}
